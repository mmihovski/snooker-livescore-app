package com.example.snookerapp;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.snookerapp.model.Player;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SharedPreference
{
    private static final String PREFERENCE = "APP_PREFERENCES";
    private static final String KEY = "favorite_players";

    public SharedPreference()
    {
        super();
    }

    private void saveFavorites(Context context, List<Player> favorites)
    {
        SharedPreferences preferences;
        SharedPreferences.Editor editor;

        preferences = context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        editor = preferences.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);

        editor.putString(KEY, jsonFavorites).apply();
    }

    public void addFavorite(Context context, Player player)
    {
        List<Player> favoritesList = getFavorites(context);

        favoritesList.add(player);
        saveFavorites(context, favoritesList);
    }

    public void removeFavorite(Context context, Player player)
    {
        List<Player> favoritesList = getFavorites(context);

        favoritesList.remove(player);
        saveFavorites(context, favoritesList);

    }

    public List<Player> getFavorites(Context context)
    {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);

        if (preferences.contains(KEY))
        {
            String jsonFavorites = preferences.getString(KEY, null);
            Gson gson = new Gson();
            Player[] favoriteItems = gson.fromJson(jsonFavorites, Player[].class);

            return new ArrayList<>(Arrays.asList(favoriteItems));
        }
        else
            return new ArrayList<>();
    }
}