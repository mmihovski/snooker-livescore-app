package com.example.snookerapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;

import java.util.Locale;

public class LocaleManager
{
    public static final String ENGLISH = "en";
    public static final String BULGARIAN = "bg";

    private static final String LANGUAGE_KEY = "language_key";

    public static Context setLocale(Context context)
    {
        return updateResources(context, getLanguagePref(context));
    }

    public static void setNewLocale(Context context,  String language)
    {
        setLanguagePref(context, language);
    }

    private static String getLanguagePref(Context context)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(LANGUAGE_KEY, ENGLISH);
    }

    private static void setLanguagePref(Context context, String localeKey)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(LANGUAGE_KEY, localeKey).apply();
    }

    private static Context updateResources(Context context, String language)
    {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Resources resources = context.getResources();
        Configuration configuration = new Configuration(resources.getConfiguration());
        configuration.setLocale(locale);

        return context.createConfigurationContext(configuration);
    }

    public static Locale getLocale(Resources resources)
    {
        Configuration config = resources.getConfiguration();
        return config.getLocales().get(0);
    }
}
