package com.example.snookerapp.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.snookerapp.HTTPHandler;
import com.example.snookerapp.R;
import com.example.snookerapp.adapter.RankingAdapter;
import com.example.snookerapp.database.SnookerDatabase;
import com.example.snookerapp.model.Ranking;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class WorldRankingFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener
{
    private List<Ranking> rankingList = new ArrayList<>();
    private RankingAdapter adapter;
    private WorldRankingAsyncTask asyncTask;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progressBar;
    private TextView messageNoInternet;
    private TextView messageNoRanking;
    private SnookerDatabase database;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_world_ranking, container, false);

        database = SnookerDatabase.getDatabase(getContext());

        progressBar = view.findViewById(R.id.world_ranking_progress_bar);
        progressBar.setVisibility(View.GONE);

        messageNoInternet = view.findViewById(R.id.world_ranking_no_internet_message);
        messageNoInternet.setVisibility(View.GONE);

        messageNoRanking = view.findViewById(R.id.world_ranking_no_ranking_message);
        messageNoRanking.setVisibility(View.GONE);

        adapter = new RankingAdapter(getContext(), rankingList);
        RecyclerView recyclerView = view.findViewById(R.id.world_ranking_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        swipeRefreshLayout = view.findViewById(R.id.world_ranking_swipe_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        getDataFromAPI();

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onRefresh()
    {
        onDestroy();

        messageNoInternet.setVisibility(View.GONE);
        messageNoRanking.setVisibility(View.GONE);

        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);

        getDataFromAPI();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        if(asyncTask != null && asyncTask.getStatus() != AsyncTask.Status.FINISHED)
            asyncTask.cancel(true);

        adapter.clearData();
    }

    private void getDataFromAPI()
    {
        if(checkNetworkConnection())
        {
            progressBar.setVisibility(View.VISIBLE);

            asyncTask = new WorldRankingAsyncTask();
            asyncTask.execute();
        }
        else  messageNoInternet.setVisibility(View.VISIBLE);
    }

    private boolean checkNetworkConnection()
    {
        if(getActivity() != null)
        {
            ConnectivityManager cm = (ConnectivityManager) this.getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

            if (cm != null) {
                NetworkInfo networkInfo = cm.getActiveNetworkInfo();
                return networkInfo != null && networkInfo.isConnected();
            }
        }
        return false;
    }

    @SuppressLint("StaticFieldLeak")
    private class WorldRankingAsyncTask extends AsyncTask<Void, Void, String>
    {
        int flag = 0;

        @Override
        protected String doInBackground(Void... params)
        {
            HTTPHandler handler = new HTTPHandler();

            String rankingURL = "http://api.snooker.org/?rt=MoneyRankings&s=2019";
            String rankingJsonString = handler.makeServiceCall(rankingURL);

            if (rankingJsonString != null)
            {
                try
                {
                    JSONArray rankingArray = new JSONArray(rankingJsonString);

                    for (int i = 0; i < rankingArray.length(); i++)
                    {
                        JSONObject rankingObject = rankingArray.getJSONObject(i);

                        int playerID = rankingObject.getInt("PlayerID");
                        String name = database.playerDAO().getPlayerNameByID(playerID);

                        Ranking ranking = new Ranking();

                        ranking.setPosition(rankingObject.getInt("Position"));
                        ranking.setPoints(rankingObject.getInt("Sum"));
                        ranking.setPlayerName(name);

                        rankingList.add(ranking);
                    }
                }
                catch (JSONException e)
                {
                    Log.e("WorldRankingFragment", "Json parsing error: " + e.getMessage());
                }
            }
            else
            {
                flag = 1;
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);

            progressBar.setVisibility(View.GONE);
            messageNoInternet.setVisibility(View.GONE);

            if (flag == 1)
                messageNoRanking.setVisibility(View.VISIBLE);
            else
                messageNoRanking.setVisibility(View.GONE);

            adapter.notifyDataSetChanged();
        }
    }
}

