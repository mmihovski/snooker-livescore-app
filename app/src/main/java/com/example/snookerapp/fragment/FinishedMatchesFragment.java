package com.example.snookerapp.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.snookerapp.HTTPHandler;
import com.example.snookerapp.R;
import com.example.snookerapp.activity.ScoresActivity;
import com.example.snookerapp.adapter.FinishedMatchesAdapter;
import com.example.snookerapp.database.SnookerDatabase;
import com.example.snookerapp.model.Match;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FinishedMatchesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener
{
    private List<Match> matchesList = new ArrayList<>();
    private FinishedMatchesAdapter adapter;
    private FinishedMatchesAsyncTask asyncTask;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progressBar;
    private TextView messageNoInternet;
    private TextView messageNoMatches;
    private SnookerDatabase database;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_finished_matches, container, false);

        database = SnookerDatabase.getDatabase(getContext());

        progressBar = view.findViewById(R.id.finished_matches_progress_bar);
        progressBar.setVisibility(View.GONE);
        messageNoInternet = view.findViewById(R.id.finished_matches_no_internet_message);
        messageNoInternet.setVisibility(View.GONE);
        messageNoMatches = view.findViewById(R.id.finished_matches_no_matches_message);
        messageNoMatches.setVisibility(View.GONE);

        RecyclerView recyclerView = view.findViewById(R.id.finished_matches_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new FinishedMatchesAdapter(getContext(), messageNoMatches);
        recyclerView.setAdapter(adapter);

        swipeRefreshLayout = view.findViewById(R.id.finished_matches_swipe_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        getDataFromAPI();
        return view;
    }

    @Override
    public void onRefresh()
    {
        onDestroy();

        messageNoInternet.setVisibility(View.GONE);
        messageNoMatches.setVisibility(View.GONE);

        if(swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);

        getDataFromAPI();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        if(asyncTask != null && asyncTask.getStatus() != AsyncTask.Status.FINISHED)
            asyncTask.cancel(true);

        adapter.clearData();
    }

    private void getDataFromAPI()
    {
        if(checkNetworkConnection())
        {
            progressBar.setVisibility(View.VISIBLE);
            asyncTask = new FinishedMatchesAsyncTask();
            asyncTask.execute();
        }
        else messageNoInternet.setVisibility(View.VISIBLE);
    }

    private boolean checkNetworkConnection()
    {
        if(getActivity() != null)
        {
            ConnectivityManager cm = (ConnectivityManager) this.getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

            if (cm != null)
            {
                NetworkInfo networkInfo = cm.getActiveNetworkInfo();
                return networkInfo != null && networkInfo.isConnected();
            }
        }
        return false;
    }

    @SuppressLint("StaticFieldLeak")
    private class FinishedMatchesAsyncTask extends AsyncTask<Void, Void, String>
    {
        private int eventID;
        private int player1ID;
        private int player2ID;
        private int winnerID;
        private int roundID;

        private int flag = 0;

        private HTTPHandler httpHandler = new HTTPHandler();

        private String matchesJsonString;
        private String playersJsonString;

        @Override
        protected String doInBackground(Void... voids)
        {
            //eventID = SplashActivity.TOURNAMENT_ID;

            if (getActivity() != null)
                eventID = ((ScoresActivity)getActivity()).getCurrentEvent();
            else eventID = 0;

            if(eventID != 0)
            {
                matchesJsonString = httpHandler.makeServiceCall("http://api.snooker.org/?t=6&e=" + eventID);
                playersJsonString = httpHandler.makeServiceCall("http://api.snooker.org/?t=9&e=" + eventID);

                try
                {
                    getMatches();
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else flag = 1;
            return null;
        }

        private void getMatches() throws JSONException
        {
            if (matchesJsonString != null)
            {
                JSONArray matchesArray = new JSONArray(matchesJsonString);

                for (int i = 0; i < matchesArray.length(); i++)
                {

                    JSONObject matchesObject = matchesArray.getJSONObject(i);

                    if (matchesObject.getInt("WinnerID") != 0)
                    {
                        Match match = new Match();

                        match.setDate(matchesObject.getString("ScheduledDate"));
                        match.setScore1(matchesObject.getInt("Score1"));
                        match.setScore2(matchesObject.getInt("Score2"));

                        player1ID = matchesObject.getInt("Player1ID");
                        player2ID = matchesObject.getInt("Player2ID");
                        winnerID = matchesObject.getInt("WinnerID");
                        roundID = matchesObject.getInt("Round");

                        getEventName(match);
                        getPlayersName(match);
                        getRoundInfo(match);

                        matchesList.add(match);
                    }
                }
            }
        }

        private void getEventName(Match match)
        {
            String name = database.eventDAO().getEventNameByID(eventID);
            match.setEvent(name);
        }

        private void getPlayersName(Match match) throws JSONException
        {
            if(playersJsonString != null)
            {
                JSONArray playersArray = new JSONArray(playersJsonString);
                boolean flag1 = false;
                boolean flag2 = false;
                for (int i = 0; i < playersArray.length(); i++)
                {
                    JSONObject playerObject = playersArray.getJSONObject(i);

                    if (playerObject.getInt("ID") == player1ID )
                    {
                        String name = playerObject.getString("FirstName") + " " + playerObject.getString("LastName");

                        if(winnerID == player1ID)
                            match.setWinner(name);

                        match.setPlayer1(name);
                        flag1 = true;
                    }

                    if (playerObject.getInt("ID") == player2ID)
                    {
                        String name = playerObject.getString("FirstName") + " " + playerObject.getString("LastName");

                        if(winnerID == player2ID)
                            match.setWinner(name);

                        match.setPlayer2(name);
                        flag2 = true;
                    }
                    if (flag1 && flag2)
                        break;
                }
            }
        }

        private void getRoundInfo(Match match)
        {
            String name = database.roundDAO().getRoundNameByID(roundID);
            match.setRound(name);

            int distance = database.eventRoundsDAO().getDistance(eventID, roundID);
            match.setDistance(distance);
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);

            progressBar.setVisibility(View.GONE);
            messageNoInternet.setVisibility(View.GONE);

            if (flag == 1)
                messageNoMatches.setVisibility(View.VISIBLE);
            else
                messageNoMatches.setVisibility(View.GONE);

            if (matchesList.isEmpty())
                messageNoMatches.setVisibility(View.VISIBLE);
            else
            {
                Collections.sort(matchesList, (match, nextMatch) -> nextMatch.getDate().compareTo(match.getDate()));

                adapter.setMatchesList(matchesList);
                adapter.notifyDataSetChanged();
            }
        }
    }
}
