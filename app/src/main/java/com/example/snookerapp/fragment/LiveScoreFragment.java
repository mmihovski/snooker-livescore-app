package com.example.snookerapp.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.snookerapp.HTTPHandler;
import com.example.snookerapp.R;
import com.example.snookerapp.activity.ScoresActivity;
import com.example.snookerapp.adapter.LiveScoreAdapter;
import com.example.snookerapp.database.SnookerDatabase;
import com.example.snookerapp.model.Match;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class LiveScoreFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener
{
    private List<Match> matchesList = new ArrayList<>();
    private LiveScoreAsyncTask asyncTask;
    private LiveScoreAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progressBar;
    private TextView messageNoInternet;
    private TextView messageNoMatches;
    private Handler handler = new Handler();
    private Runnable runnable;
    private SnookerDatabase database;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_live_score, container, false);

        database = SnookerDatabase.getDatabase(getContext());

        progressBar = view.findViewById(R.id.live_score_progres_bar);
        progressBar.setVisibility(View.GONE);
        messageNoInternet = view.findViewById(R.id.live_score_no_internet_message);
        messageNoInternet.setVisibility(View.GONE);
        messageNoMatches = view.findViewById(R.id.live_score_no_matches_message);
        messageNoMatches.setVisibility(View.GONE);

        adapter = new LiveScoreAdapter(getContext());

        RecyclerView recyclerView = view.findViewById(R.id.live_score_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        swipeRefreshLayout = view.findViewById(R.id.live_score_swipe_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        getDataFromAPI();

        runnable = new Runnable()
        {
            @Override
            public void run()
            {
                onRefresh();
                handler.postDelayed(this,30000);
            }
        };
        handler.postDelayed(runnable,30000);

        return view;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        if(asyncTask != null && asyncTask.getStatus() != AsyncTask.Status.FINISHED)
            asyncTask.cancel(true);

        handler.removeCallbacks(runnable);
        adapter.clearData();
    }

    @Override
    public void onRefresh()
    {
        onDestroy();

        messageNoInternet.setVisibility(View.GONE);
        messageNoMatches.setVisibility(View.GONE);

        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);

        getDataFromAPI();
    }

    private void getDataFromAPI()
    {
        if(checkNetworkConnection())
        {
            progressBar.setVisibility(View.VISIBLE);
            asyncTask = new LiveScoreAsyncTask();
            asyncTask.execute();
        }
        else  messageNoInternet.setVisibility(View.VISIBLE);
    }

    private boolean checkNetworkConnection()
    {
        if(getActivity() != null)
        {
            ConnectivityManager cm = (ConnectivityManager) this.getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

            if (cm != null)
            {
                NetworkInfo networkInfo = cm.getActiveNetworkInfo();
                return networkInfo != null && networkInfo.isConnected();
            }
        }
        return false;
    }

    @SuppressLint("StaticFieldLeak")
    private class LiveScoreAsyncTask extends AsyncTask<Void, Void, String>
    {
        private int eventID;
        private int player1ID;
        private int player2ID;
        private int roundID;

        private int flag = 0;

        private HTTPHandler httpHandler = new HTTPHandler();

        private String matchesJsonString;
        private String playersJsonString;

        @Override
        protected String doInBackground(Void... voids)
        {
/*
            matchesJsonString = httpHandler.makeServiceCall("http://10.0.2.2:3000/livescores");

            if(matchesJsonString != null)
            {
                try
                {
                    JSONArray livescoreArray = new JSONArray(matchesJsonString);

                    for (int i = 0; i < livescoreArray.length(); i++)
                    {
                        JSONObject livescoreObject = livescoreArray.getJSONObject(i);

                        Match match = new Match();

                        match.setScore1(livescoreObject.getInt("Score1"));
                        match.setScore2(livescoreObject.getInt("Score2"));
                        match.setPlayer1(livescoreObject.getString("Player1"));
                        match.setPlayer2(livescoreObject.getString("Player2"));
                        match.setDistance(livescoreObject.getInt("Distance"));
                        match.setRound(livescoreObject.getString("Round"));
                        match.setEvent(livescoreObject.getString("Event"));
                        match.setOnBreak(livescoreObject.getBoolean("OnBreak"));

                        matchesList.add(match);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
*/



            if (getActivity() != null)
                eventID = ((ScoresActivity)getActivity()).getCurrentEvent();
            else eventID = 0;

            matchesJsonString = httpHandler.makeServiceCall("http://api.snooker.org/?t=7");

            if(matchesJsonString == null || eventID == 0)
                flag = 1;
            else
            {
                playersJsonString = httpHandler.makeServiceCall("http://api.snooker.org/?t=9&e=" + eventID);

                try
                {
                    getMatches();
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
            return null;
        }

        private void getMatches() throws JSONException
        {
            if(matchesJsonString != null)
            {
                JSONArray livescoreArray = new JSONArray(matchesJsonString);

                for (int i = 0; i < livescoreArray.length(); i++)
                {
                    JSONObject livescoreObject = livescoreArray.getJSONObject(i);

                    if(livescoreObject.getInt("EventID") != eventID)
                        continue;

                    Match match = new Match();

                    match.setScore1(livescoreObject.getInt("Score1"));
                    match.setScore2(livescoreObject.getInt("Score2"));
                    match.setOnBreak(livescoreObject.getBoolean("OnBreak"));

                    player1ID = livescoreObject.getInt("Player1ID");
                    player2ID = livescoreObject.getInt("Player2ID");
                    roundID = livescoreObject.getInt("Round");

                    getEventName(match);
                    getPlayersName(match);
                    getRoundInfo(match);

                    matchesList.add(match);
                }
            }
        }

        private void getEventName(Match match)
        {
            String name = database.eventDAO().getEventNameByID(eventID);
            match.setEvent(name);
        }

        private void getPlayersName(Match match) throws JSONException
        {
            if(playersJsonString != null)
            {
                JSONArray playersArray = new JSONArray(playersJsonString);
                int flag1 = 0;
                int flag2 = 0;
                for (int i = 0; i < playersArray.length(); i++)
                {
                    JSONObject playerObject = playersArray.getJSONObject(i);
                    if (playerObject.getInt("ID") == player1ID)
                    {
                        match.setPlayer1(playerObject.getString("FirstName") + " " + playerObject.getString("LastName"));
                        flag1 = 1;
                    }

                    if (playerObject.getInt("ID") == player2ID)
                    {
                        match.setPlayer2(playerObject.getString("FirstName") + " " + playerObject.getString("LastName"));
                        flag2 = 1;
                    }
                    if (flag1 == 1 && flag2 == 1)
                        break;
                }
            }
        }

        private void getRoundInfo(Match match)
        {
            String name = database.roundDAO().getRoundNameByID(roundID);
            match.setRound(name);

            int distance = database.eventRoundsDAO().getDistance(eventID, roundID);
            match.setDistance(distance);
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);

            progressBar.setVisibility(View.GONE);
            messageNoInternet.setVisibility(View.GONE);

            if(flag == 1)
                messageNoMatches.setVisibility(View.VISIBLE);
            else if(matchesList.isEmpty())
            {
                messageNoMatches.setVisibility(View.VISIBLE);
            }

            adapter.setMatchesList(matchesList);
            adapter.notifyDataSetChanged();
        }
    }
}

