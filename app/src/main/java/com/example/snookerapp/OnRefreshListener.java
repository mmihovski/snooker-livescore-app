package com.example.snookerapp;

public interface OnRefreshListener
{
    void onRefresh();
}
