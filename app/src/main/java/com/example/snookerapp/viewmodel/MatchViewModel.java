package com.example.snookerapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.snookerapp.model.Match;
import com.example.snookerapp.repository.MatchRepository;

import java.util.List;

public class MatchViewModel extends AndroidViewModel
{
    private MatchRepository matchRepository;
    public MatchViewModel(@NonNull Application application)
    {
        super(application);
        matchRepository = new MatchRepository(application);
    }

    public LiveData<List<Match>> getAllMatchesByPlayerId(int playerID)
    {
        return matchRepository.getAllMatchesByPlayerId(playerID);
    }

    public LiveData<List<Match>> getAllMatchesByEventId(int eventID)
    {
        return matchRepository.getAllMatchesByEventId(eventID);
    }
}
