package com.example.snookerapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.snookerapp.model.PlayerPerformance;
import com.example.snookerapp.repository.StatisticsRepository;

import java.util.List;

public class StatisticsViewModel extends AndroidViewModel
{
    private StatisticsRepository playerPerformanceRepository;

    public StatisticsViewModel(@NonNull Application application)
    {
        super(application);
        playerPerformanceRepository = new StatisticsRepository(application);
    }

    public LiveData<List<PlayerPerformance>> getPlayersStatistics()
    {
        return playerPerformanceRepository.getMatchStatistics();
    }

    public LiveData<List<PlayerPerformance>> getStatisticsOfPlayersWithMostWinningEvents()
    {
        return playerPerformanceRepository.getStatisticsOfPlayersWithMostWinningEvents();
    }

    public LiveData<List<PlayerPerformance>> getStatisticsOfPlayersWithMostWinningMatches()
    {
        return playerPerformanceRepository.getStatisticsOfPlayersWithMostWinningMatches();
    }

    public LiveData<List<PlayerPerformance>> getStatisticsOfPlayersWithMostLosingMatches()
    {
        return playerPerformanceRepository.getStatisticsOfPlayersWithMostLosingMatches();
    }

    public LiveData<List<PlayerPerformance>> etStatisticsOfCountriesWithMostWinningMatches()
    {
        return playerPerformanceRepository.etStatisticsOfCountriesWithMostWinningMatches();
    }
}
