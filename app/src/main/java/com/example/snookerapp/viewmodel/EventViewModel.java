package com.example.snookerapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.snookerapp.model.Event;
import com.example.snookerapp.repository.EventRepository;

import java.util.List;

public class EventViewModel extends AndroidViewModel
{
    private EventRepository eventRepository;

    public EventViewModel(@NonNull Application application)
    {
        super(application);
        eventRepository = new EventRepository(application);
    }

    public LiveData<List<Event>> getAllEvents()
    {
        return eventRepository.getAllEvents();
    }

    public LiveData<List<Event>> getRankingEvents()
    {
        return eventRepository.getRankingEvents();
    }

    public LiveData<List<Event>> getInvitationalEvents()
    {
        return eventRepository.getInvitationalEvents();
    }

    public LiveData<List<Event>> getQualifyingEvents()
    {
        return eventRepository.getQualifyingEvents();
    }
}
