package com.example.snookerapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.snookerapp.model.Player;
import com.example.snookerapp.repository.PlayerRepository;

import java.util.List;

public class PlayerViewModel extends AndroidViewModel
{
    private PlayerRepository playerRepository;

    public PlayerViewModel(@NonNull Application application)
    {
        super(application);
        playerRepository = new PlayerRepository(application);
    }

    public LiveData<List<Player>> getPlayerListSortById()
    {
        return playerRepository.getAllPlayersSortById();
    }

    public LiveData<List<Player>> getPlayerListSortByFirstName()
    {
        return playerRepository.getAllPlayersSortByFirstName();
    }

    public LiveData<List<Player>> getPlayerListSortByLastName()
    {
        return playerRepository.getAllPlayersSortByLastName();
    }

    public LiveData<List<Player>> getPlayerListSortByCountry()
    {
        return playerRepository.getAllPlayersSortByCountry();
    }
}
