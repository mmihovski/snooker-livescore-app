package com.example.snookerapp.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.snookerapp.OnRefreshListener;
import com.example.snookerapp.R;
import com.example.snookerapp.SharedPreference;
import com.example.snookerapp.adapter.PlayerAdapter;
import com.example.snookerapp.viewmodel.PlayerViewModel;

public class PlayersActivity extends BaseActivity implements OnRefreshListener
{
    private PlayerViewModel viewModel;
    private PlayerAdapter allPlayersAdapter;
    private PlayerAdapter favoritePlayersAdapter;
    private RecyclerView favoritePlayersRecyclerView;
    private SharedPreference sharedPreference = new SharedPreference();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        FrameLayout container = findViewById(R.id.base_container);
        getLayoutInflater().inflate(R.layout.content_players, container);

        navigationView.getMenu().findItem(R.id.nav_players).setCheckable(true).setChecked(true);

        allPlayersAdapter = new PlayerAdapter(this);
        favoritePlayersAdapter = new PlayerAdapter(this);
        viewModel = ViewModelProviders.of(this).get(PlayerViewModel.class);

        RecyclerView allPlayersRecyclerView = findViewById(R.id.players_recycler_view);
        allPlayersRecyclerView.setAdapter(allPlayersAdapter);
        allPlayersRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        allPlayersRecyclerView.setNestedScrollingEnabled(false);

        favoritePlayersRecyclerView = findViewById(R.id.player_favorites_recycler_view);
        favoritePlayersRecyclerView.setAdapter(favoritePlayersAdapter);
        favoritePlayersRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        favoritePlayersRecyclerView.setNestedScrollingEnabled(false);

        viewModel.getPlayerListSortById().observe(this, allPlayersAdapter::setAllPlayersList);
        favoritePlayersAdapter.setAllPlayersList(sharedPreference.getFavorites(this));
    }


    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = findViewById(R.id.base_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else
            finishActivity();
    }

    @Override
    public void onRefresh()
    {
        viewModel.getPlayerListSortById().observe(this, allPlayersAdapter::setAllPlayersList);
        favoritePlayersAdapter.setAllPlayersList(sharedPreference.getFavorites(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.players_search_view, menu);
        getMenuInflater().inflate(R.menu.players_sorting_menu, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.player_search_bar).getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setQueryHint(getResources().getString(R.string.player_search));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                favoritePlayersRecyclerView.setVisibility(View.GONE);
                allPlayersAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query)
            {
                favoritePlayersRecyclerView.setVisibility(View.GONE);
                allPlayersAdapter.getFilter().filter(query);
                return false;
            }
        });

        searchView.setOnCloseListener(() ->
        {
            favoritePlayersRecyclerView.setVisibility(View.VISIBLE);
            return false;
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        int id = item.getItemId();

        switch (id)
        {
            case R.id.player_sort_by_country:
                viewModel.getPlayerListSortByCountry().observe(this, allPlayersAdapter::setAllPlayersList);
                return true;

            case R.id.player_sort_by_first_name:
                viewModel.getPlayerListSortByFirstName().observe(this, allPlayersAdapter::setAllPlayersList);
                return true;

            case R.id.player_sort_by_last_name:
                viewModel.getPlayerListSortByLastName().observe(this, allPlayersAdapter::setAllPlayersList);
                return true;

            case R.id.player_default_sorting:
                viewModel.getPlayerListSortById().observe(this, allPlayersAdapter::setAllPlayersList);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}


