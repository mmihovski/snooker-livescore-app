package com.example.snookerapp.activity;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.snookerapp.R;
import com.example.snookerapp.fragment.WorldRankingFragment;
import com.example.snookerapp.fragment.YearRankingFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class RankingActivity extends BaseActivity
{
    final Fragment WR_fragment = new WorldRankingFragment();
    final Fragment YR_fragment = new YearRankingFragment();

    final FragmentManager fragmentManager = this.getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        FrameLayout container = findViewById(R.id.base_container);
        getLayoutInflater().inflate(R.layout.content_ranking, container);

        navigationView.getMenu().findItem(R.id.nav_ranking).setCheckable(true).setChecked(true);

        fragmentManager.beginTransaction().add(R.id.ranking_container, WR_fragment, "world ranking").show(WR_fragment).commit();
        fragmentManager.beginTransaction().add(R.id.ranking_container, YR_fragment, "year ranking").hide(YR_fragment).commit();

        setTitle(R.string.world_ranking);

        BottomNavigationView navigation =  findViewById(R.id.ranking_bottom_navigation_view);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = item ->
    {
        switch (item.getItemId())
        {
            case R.id.bottom_navigation_world_ranking:
                fragmentManager.beginTransaction().hide(YR_fragment).show(WR_fragment).commit();
                setTitle(R.string.world_ranking);
                return true;

            case R.id.bottom_navigation_year_ranking:
                fragmentManager.beginTransaction().hide(WR_fragment).show(YR_fragment).commit();
                setTitle(R.string.year_ranking);
                return true;
        }

        return false;
    };

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = findViewById(R.id.base_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else
            finishActivity();
    }
}
