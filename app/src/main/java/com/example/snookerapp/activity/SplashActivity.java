package com.example.snookerapp.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Intent mainActivity = new Intent(this, HomeActivity.class);
        startActivity(mainActivity);

        finish();
    }
}
