package com.example.snookerapp.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.snookerapp.R;
import com.example.snookerapp.database.SnookerDatabase;
import com.example.snookerapp.fragment.FinishedMatchesFragment;
import com.example.snookerapp.fragment.LiveScoreFragment;
import com.example.snookerapp.fragment.UpcomingMatchesFragment;
import com.google.android.material.tabs.TabLayout;

public class ScoresActivity extends BaseActivity
{
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        FrameLayout container = findViewById(R.id.base_container);
        getLayoutInflater().inflate(R.layout.content_scores, container);

        navigationView.getMenu().findItem(R.id.nav_scores).setCheckable(true).setChecked(true);

        SnookerDatabase database = SnookerDatabase.getDatabase(this);
        if(getCurrentEvent()!=0)
            new Thread(() -> setTitle(database.eventDAO().getEventNameByID(getCurrentEvent()))).start();

        TabLayout tabLayout = findViewById(R.id.scores_tab);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        TabsAdapter tabsAdapter = new TabsAdapter(getSupportFragmentManager(), tabLayout.getTabCount());

        viewPager = findViewById(R.id.scores_pager);
        viewPager.setAdapter(tabsAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab)
            {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab)
            {

            }
        });
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = findViewById(R.id.base_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else
            finishActivity();
    }

    public int getCurrentEvent()
    {
        SharedPreferences sp = getSharedPreferences("APP_PREFERENCES", MODE_PRIVATE);
        return sp.getInt("current_tournament", -1);
    }

    private class TabsAdapter extends FragmentStatePagerAdapter
    {
        private int tabsNumber;

        private TabsAdapter(FragmentManager fragmentManager, int tabsNumber)
        {
            super(fragmentManager);
            this.tabsNumber = tabsNumber;
        }

        @Override
        public Fragment getItem(int position)
        {
            switch (position)
            {
                case 0:
                    return new LiveScoreFragment();

                case 1:
                    return new UpcomingMatchesFragment();

                case 2:
                    return new FinishedMatchesFragment();
            }
            return null;
        }

        @Override
        public int getCount()
        {
            return tabsNumber;
        }
    }
}


