package com.example.snookerapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;

import com.example.snookerapp.LocaleManager;
import com.example.snookerapp.R;

import java.util.Locale;

import static android.content.pm.PackageManager.GET_META_DATA;

public class SettingsActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_settings);
        resetTitles();

        RadioGroup radioGroup = findViewById(R.id.settings_radio_group);
        RadioButton radioButtonEnglish = findViewById(R.id.settings_radio_button_english);
        RadioButton radioButtonBulgarian = findViewById(R.id.settings_radio_button_bulgarian);

        Locale currentLocale = LocaleManager.getLocale(getResources());

        if(currentLocale.equals(new Locale(LocaleManager.ENGLISH)))
        {
            radioButtonEnglish.setChecked(true);
        }
        else if(currentLocale.equals(new Locale(LocaleManager.BULGARIAN)))
        {
            radioButtonBulgarian.setChecked(true);
        }
        radioGroup.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId)
    {
        switch (checkedId)
        {
            case R.id.settings_radio_button_english:
                setNewLocale(this, LocaleManager.ENGLISH);
                break;

            case R.id.settings_radio_button_bulgarian:
                setNewLocale(this, LocaleManager.BULGARIAN);
                break;
        }
    }

    private void setNewLocale(AppCompatActivity mContext,  String language)
    {
        LocaleManager.setNewLocale(this, language);
        Intent intent = mContext.getIntent();
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void onBackPressed()
    {
        Intent home = new Intent(this, HomeActivity.class);
        startActivity(home);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        onBackPressed();
        return true;
    }

    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    private void resetTitles()
    {
        try
        {
            ActivityInfo info = getPackageManager().getActivityInfo(getComponentName(), GET_META_DATA);
            if (info.labelRes != 0)
                setTitle(info.labelRes);

        }
        catch (PackageManager.NameNotFoundException e)
        {
            e.printStackTrace();
        }
    }
}

