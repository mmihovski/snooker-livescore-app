package com.example.snookerapp.activity;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.snookerapp.R;
import com.example.snookerapp.adapter.EventAdapter;
import com.example.snookerapp.viewmodel.EventViewModel;
import com.google.android.material.chip.ChipGroup;

public class EventsActivity extends BaseActivity implements ChipGroup.OnCheckedChangeListener
{
    private EventViewModel viewModel;
    private EventAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        FrameLayout container = findViewById(R.id.base_container);
        getLayoutInflater().inflate(R.layout.content_events, container);

        navigationView.getMenu().findItem(R.id.nav_calendar).setCheckable(true).setChecked(true);

        adapter = new EventAdapter(this);

        RecyclerView recyclerView = findViewById(R.id.events_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        viewModel = ViewModelProviders.of(this).get(EventViewModel.class);
        viewModel.getAllEvents().observe(this, adapter::setEventList);

        ChipGroup chipGroup = findViewById(R.id.events_chip_group);
        chipGroup.setOnCheckedChangeListener(this);

    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = findViewById(R.id.base_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else
        {
            finishActivity();
        }
    }

    @Override
    public void onCheckedChanged(ChipGroup chipGroup, int i)
    {
        switch (i)
        {
            case R.id.events_chip_all:
                viewModel.getAllEvents().observe(EventsActivity.this, adapter::setEventList);
                break;

            case R.id.events_chip_ranking:
                viewModel.getRankingEvents().observe(EventsActivity.this, adapter::setEventList);
                break;

            case R.id.events_chip_invitational:
                viewModel.getInvitationalEvents().observe(EventsActivity.this, adapter::setEventList);
                break;

            case R.id.events_chip_qualifying:
                viewModel.getQualifyingEvents().observe(EventsActivity.this, adapter::setEventList);
                break;

            default:
                adapter.clearData();
        }
    }
}
