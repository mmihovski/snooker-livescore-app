package com.example.snookerapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.snookerapp.R;

public class HomeActivity extends BaseActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        FrameLayout container = findViewById(R.id.base_container);
        getLayoutInflater().inflate(R.layout.content_home, container);

        navigationView.getMenu().findItem(R.id.nav_home).setCheckable(true).setChecked(true);

        findViewById(R.id.home_menu_scores).setOnClickListener(view ->
        {
            Intent scores = new Intent(this, ScoresActivity.class);
            startActivity(scores);
        });

        findViewById(R.id.home_menu_tournament).setOnClickListener(view ->
        {
            Intent event = new Intent(this, MatchesActivity.class);
            int eventID = getSharedPreferences("APP_PREFERENCES", MODE_PRIVATE).getInt("current_tournament", -1);
            event.putExtra("EventID", eventID);
            startActivity(event);
        });

        findViewById(R.id.home_menu_calendar).setOnClickListener(view ->
        {
            Intent calendar = new Intent(this, EventsActivity.class);
            startActivity(calendar);
        });

        findViewById(R.id.home_menu_players).setOnClickListener(view ->
        {
            Intent player= new Intent(this, PlayersActivity.class);
            startActivity(player);
        });

        findViewById(R.id.home_menu_ranking).setOnClickListener(view ->
        {
            Intent ranking = new Intent(this, RankingActivity.class);
            startActivity(ranking);
        });

        findViewById(R.id.home_menu_statistics).setOnClickListener(view ->
        {
            Intent statistics = new Intent(this, StatisticsActivity.class);
            startActivity(statistics);
        });
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = findViewById(R.id.base_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else
        {
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        }
    }
}
