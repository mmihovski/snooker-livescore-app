package com.example.snookerapp.activity;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.snookerapp.LocaleManager;
import com.example.snookerapp.R;
import com.example.snookerapp.adapter.FinishedMatchesAdapter;
import com.example.snookerapp.database.SnookerDatabase;
import com.example.snookerapp.viewmodel.MatchViewModel;

import static android.content.pm.PackageManager.GET_META_DATA;

public class MatchesActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matches);

        resetTitles();

        final SnookerDatabase database = SnookerDatabase.getDatabase(this);

        TextView textView = findViewById(R.id.matches_message);

        RecyclerView recyclerView = findViewById(R.id.matches_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        FinishedMatchesAdapter adapter = new FinishedMatchesAdapter(this, textView);
        recyclerView.setAdapter(adapter);
        MatchViewModel matchViewModel = ViewModelProviders.of(this).get(MatchViewModel.class);


        if(getIntent().getExtras() != null)
        {
            if (getIntent().getExtras().containsKey("PlayerID"))
            {
                int playerID = getIntent().getExtras().getInt("PlayerID");
                matchViewModel.getAllMatchesByPlayerId(playerID).observe(this, adapter::setMatchesList);
                new Thread(() -> setTitle(database.playerDAO().getPlayerNameByID(playerID))).start();
            }
            else if (getIntent().getExtras().containsKey("EventID"))
            {
                int eventID = getIntent().getExtras().getInt("EventID");
                matchViewModel.getAllMatchesByEventId(eventID).observe(this, adapter::setMatchesList);
                if(eventID != 0)
                    new Thread(() -> setTitle(database.eventDAO().getEventNameByID(eventID))).start();
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        onBackPressed();
        return true;
    }

    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    protected void resetTitles()
    {
        try
        {
            ActivityInfo info = getPackageManager().getActivityInfo(getComponentName(), GET_META_DATA);
            if (info.labelRes != 0)
            {
                setTitle(info.labelRes);
            }
        }
        catch (PackageManager.NameNotFoundException e)
        {
            e.printStackTrace();
        }
    }
}
