package com.example.snookerapp.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.Spinner;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.snookerapp.R;
import com.example.snookerapp.adapter.StatisticsAdapter;
import com.example.snookerapp.viewmodel.StatisticsViewModel;

public class StatisticsActivity extends BaseActivity implements AdapterView.OnItemSelectedListener
{
    private StatisticsViewModel viewModel;
    private StatisticsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        FrameLayout container = findViewById(R.id.base_container);
        getLayoutInflater().inflate(R.layout.content_statistics, container);

        navigationView.getMenu().findItem(R.id.nav_statistics).setCheckable(true).setChecked(true);

        adapter = new StatisticsAdapter(this);

        RecyclerView recyclerView = findViewById(R.id.statistics_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        Spinner spinner = findViewById(R.id.statistics_spinner);
        spinner.setOnItemSelectedListener(this);

        viewModel = ViewModelProviders.of(this).get(StatisticsViewModel.class);
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = findViewById(R.id.base_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else    finishActivity();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l)
    {
        switch (position)
        {
            case 0:
                viewModel.getPlayersStatistics().observe(this, adapter::setStatisticsList);
                break;

            case 1:
                viewModel.getStatisticsOfPlayersWithMostWinningMatches().observe(this, adapter::setStatisticsList);
                break;

            case 2:
                viewModel.getStatisticsOfPlayersWithMostLosingMatches().observe(this, adapter::setStatisticsList);
                break;

            case 3:
                viewModel.getStatisticsOfPlayersWithMostWinningEvents().observe(this, adapter::setStatisticsList);
                break;

            case 4:
                viewModel.etStatisticsOfCountriesWithMostWinningMatches().observe(this, adapter::setStatisticsList);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView)
    {
        viewModel.getPlayersStatistics().observe(this, adapter::setStatisticsList);
    }
}


