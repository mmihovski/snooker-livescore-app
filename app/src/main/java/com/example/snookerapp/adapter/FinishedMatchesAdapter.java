package com.example.snookerapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.snookerapp.R;
import com.example.snookerapp.model.Match;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

public class FinishedMatchesAdapter extends RecyclerView.Adapter<FinishedMatchesAdapter.ViewHolder>
{
    private final LayoutInflater inflater;
    private List<Match> matchesList;
    private TextView message;

    public FinishedMatchesAdapter(Context context, TextView message)
    {
        inflater = LayoutInflater.from(context);
        this.message = message;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = inflater.inflate(R.layout.view_element_finished_match, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        Match current = matchesList.get(position);

        String distance = "(" + (current.getDistance() * 2 - 1) + ")";
        String date = parseDate(current.getDate());
        String score1 = Integer.toString(current.getScore1());
        String score2 = Integer.toString(current.getScore2());
        String winner = current.getWinner();
        String player1 = current.getPlayer1();
        String player2 = current.getPlayer2();

        if(player1.equals(winner))
        {
            holder.winner.setText(player1);
            holder.looser.setText(player2);
            holder.scoreWinner.setText(score1);
            holder.scoreLooser.setText(score2);
        }
        else if(player2.equals(winner))
        {
            holder.winner.setText(player2);
            holder.looser.setText(player1);
            holder.scoreWinner.setText(score2);
            holder.scoreLooser.setText(score1);
        }

        holder.date.setText(date);
        holder.distance.setText(distance);
        holder.round.setText(current.getRound());
        holder.event.setText(current.getEvent());
    }

    @Override
    public int getItemCount()
    {
        if (matchesList != null)
            return matchesList.size();
        else return 0;
    }

    public void setMatchesList(List<Match> matchesList)
    {
        this.matchesList = matchesList;

        if(matchesList.isEmpty())
        {
            message.setVisibility(View.VISIBLE);
        }
        notifyDataSetChanged();
    }

    public void clearData()
    {
        if(matchesList != null)
        {
            int size = this.matchesList.size();
            matchesList.clear();
            notifyItemRangeRemoved(0, size);
        }
    }

    private String parseDate(String date)
    {
        final DateTimeFormatter OLD_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        final DateTimeFormatter NEW_FORMATTER = DateTimeFormatter.ofPattern("dd MMM yyyy");

        try
        {
            LocalDate localDate = LocalDate.parse(date, OLD_FORMATTER);
            return localDate.format(NEW_FORMATTER);
        }
        catch (DateTimeParseException exception)
        {
            return inflater.getContext().getResources().getString(R.string.unknown_date);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView winner, looser, event, round, distance, date, scoreWinner, scoreLooser;

        private ViewHolder(View view)
        {
            super(view);
            winner = view.findViewById(R.id.finished_match_winner);
            looser = view.findViewById(R.id.finished_match_losser);
            event = view.findViewById(R.id.finished_match_event);
            round = view.findViewById(R.id.finished_match_round);
            distance = view.findViewById(R.id.finished_match_distance);
            date = view.findViewById(R.id.finished_match_date);
            scoreWinner = view.findViewById(R.id.finished_match_score_winner);
            scoreLooser = view.findViewById(R.id.finished_match_score_looser);
        }
    }
}
