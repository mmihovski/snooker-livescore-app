package com.example.snookerapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.snookerapp.R;
import com.example.snookerapp.model.Match;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

public class UpcomingMatchesAdapter extends RecyclerView.Adapter<UpcomingMatchesAdapter.ViewHolder>
{
    private final LayoutInflater inflater;
    private List<Match> matchesList;

    public UpcomingMatchesAdapter(Context context)
    {
        inflater = LayoutInflater.from(context);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = inflater.inflate(R.layout.view_element_upcoming_match, parent, false);
        return new ViewHolder(itemView);
    }

    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        Match current = matchesList.get(position);

        String distance = "(" + (current.getDistance()*2-1) + ")";
        String date = parseDate(current.getDate());

        holder.player1.setText(current.getPlayer1());
        holder.player2.setText(current.getPlayer2());
        holder.date.setText(date);
        holder.distance.setText(distance);
        holder.round.setText(current.getRound());
        holder.event.setText(current.getEvent());
    }

    @Override
    public int getItemCount()
    {
        if (matchesList != null)
            return matchesList.size();
        else return 0;
    }

    public void clearData()
    {
        if(matchesList != null)
        {
            int size = this.matchesList.size();
            matchesList.clear();
            notifyItemRangeRemoved(0, size);
        }
    }

    public void setMatchesList(List<Match> matchesList)
    {
        this.matchesList = matchesList;
        notifyDataSetChanged();
    }
    private String parseDate(@NonNull String date)
    {
        final DateTimeFormatter OLD_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        final DateTimeFormatter NEW_FORMATTER = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm");
        final DateTimeFormatter OLD_FORMATTER1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        final DateTimeFormatter NEW_FORMATTER1 = DateTimeFormatter.ofPattern("dd MMM yyyy");
        ZoneId oldZone = ZoneId.of("GMT");
        ZoneId newZone = ZoneId.systemDefault();

        try
        {
            LocalDateTime oldDateTime = LocalDateTime.parse(date, OLD_FORMATTER);
            LocalDateTime newDateTime = oldDateTime.atZone(oldZone).withZoneSameInstant(newZone).toLocalDateTime();
            return newDateTime.format(NEW_FORMATTER);
        }
        catch (DateTimeParseException exception1)
        {
            try
            {
                LocalDate newDater = LocalDate.parse(date, OLD_FORMATTER1);
                return newDater.format(NEW_FORMATTER1);
            }
            catch (DateTimeParseException exception2)
            {
                return inflater.getContext().getResources().getString(R.string.unknown_date);
            }

        }
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView player1, player2, event, round, distance, date;

        private ViewHolder(View v)
        {
            super(v);
            player1 = v.findViewById(R.id.upcoming_matches_player1);
            player2 = v.findViewById(R.id.upcoming_matches_player2);
            event = v.findViewById(R.id.upcoming_matches_event);
            round = v.findViewById(R.id.upcoming_matches_round);
            distance = v.findViewById(R.id.upcoming_matches_distance);
            date = v.findViewById(R.id.upcoming_matches_date);
        }
    }
}
