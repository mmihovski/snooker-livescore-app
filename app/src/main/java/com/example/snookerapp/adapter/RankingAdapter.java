package com.example.snookerapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.snookerapp.R;
import com.example.snookerapp.model.Ranking;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class RankingAdapter extends RecyclerView.Adapter<RankingAdapter.ViewHolder>
{
    private final LayoutInflater inflater;
    private List<Ranking> rankingsList;

    public RankingAdapter (Context context, List<Ranking> rankingList)
    {
        inflater = LayoutInflater.from(context);
        this.rankingsList = rankingList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = inflater.inflate(R.layout.view_element_ranking, parent, false);
        return new ViewHolder(itemView);
    }

    public void onBindViewHolder(@NonNull ViewHolder holder, int pos)
    {
        Ranking ranking = rankingsList.get(pos);

        String points = "£ " + NumberFormat.getNumberInstance(Locale.US).format(ranking.getPoints());
        String position = Integer.toString(ranking.getPosition());
        String name = ranking.getPlayerName();

        holder.position.setText(position);
        holder.name.setText(name);
        holder.points.setText(points);
    }


    @Override
    public int getItemCount()
    {
        if (rankingsList != null)
            return rankingsList.size();
        else return 0;
    }

    public void clearData()
    {
        if(rankingsList != null)
        {
            int size = this.rankingsList.size();
            rankingsList.clear();
            notifyItemRangeRemoved(0, size);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView position, name, points ;

        private ViewHolder(View view)
        {
            super(view);
            position = view.findViewById(R.id.ranking_position);
            name = view.findViewById(R.id.ranking_name);
            points = view.findViewById(R.id.ranking_points);
        }
    }
}
