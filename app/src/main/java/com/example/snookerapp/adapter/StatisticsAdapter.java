package com.example.snookerapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.snookerapp.R;
import com.example.snookerapp.model.PlayerPerformance;

import java.util.List;

public class StatisticsAdapter extends RecyclerView.Adapter<StatisticsAdapter.ViewHolder>
{
    private final LayoutInflater inflater;
    private List<PlayerPerformance> statisticsList;

    public StatisticsAdapter(Context context)
    {
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = inflater.inflate(R.layout.view_element_player_statistics, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        PlayerPerformance current = statisticsList.get(position);

        String name = current.getName();
        String value = current.getValue();

        if(value != null)
        {
            holder.name.setText(name);
            holder.value.setText(value);
        }
    }

    public void setStatisticsList(List<PlayerPerformance> playerPerformancesList)
    {
        this.statisticsList = playerPerformancesList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount()
    {
        if (statisticsList != null)
            return statisticsList.size();
        else return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView name, value;

        private ViewHolder(View view)
        {
            super(view);
            name =  view.findViewById(R.id.statistics_name);
            value = view.findViewById(R.id.statistics_value);
        }
    }
}

