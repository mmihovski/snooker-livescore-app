package com.example.snookerapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.snookerapp.R;
import com.example.snookerapp.model.Match;

import java.util.List;

public class LiveScoreAdapter extends RecyclerView.Adapter<LiveScoreAdapter.ViewHolder>
{
    private final LayoutInflater inflater;
    private List<Match> matchesList;

    public LiveScoreAdapter(Context context)
    {
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = inflater.inflate(R.layout.view_element_live_match, parent, false);
        return new ViewHolder(itemView);
    }

    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        Match current = matchesList.get(position);

        String distance = "(" + (current.getDistance()*2-1) + ")";
        String score1 = Integer.toString(current.getScore1());
        String score2 = Integer.toString(current.getScore2());

        if(current.isOnBreak())
        {
            holder.onBreak.setText("BREAK");
            holder.onBreak.setBackgroundColor(inflater.getContext().getColor(R.color.colorGrey));
        }
        else
        {
            holder.onBreak.setText("LIVE");
            holder.onBreak.setBackgroundColor(inflater.getContext().getColor(R.color.colorRedBall));
        }

        holder.player1.setText(current.getPlayer1());
        holder.player2.setText(current.getPlayer2());
        holder.score1.setText(score1);
        holder.score2.setText(score2);
        holder.distance.setText(distance);
        holder.round.setText(current.getRound());
        holder.event.setText(current.getEvent());
    }

    @Override
    public int getItemCount()
    {
        if (matchesList != null)
            return matchesList.size();
        else return 0;
    }

    public void clearData()
    {
        if(matchesList != null)
        {
            int size = this.matchesList.size();
            matchesList.clear();
            notifyItemRangeRemoved(0, size);
        }
    }

    public void setMatchesList(List<Match> matchesList)
    {
        this.matchesList = matchesList;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView player1, player2, event, round, distance, score1, score2, onBreak;

        private ViewHolder(View view)
        {
            super(view);
            player1 = view.findViewById(R.id.live_score_player1);
            player2 = view.findViewById(R.id.live_score_player2);
            event = view.findViewById(R.id.live_score_event);
            round = view.findViewById(R.id.live_score_round);
            distance = view.findViewById(R.id.live_score_distance);
            score1 = view.findViewById(R.id.live_score_score1);
            score2 = view.findViewById(R.id.live_score_score2);
            onBreak = view.findViewById(R.id.live_score_on_break);
        }
    }
}
