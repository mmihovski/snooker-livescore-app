package com.example.snookerapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.snookerapp.OnRefreshListener;
import com.example.snookerapp.R;
import com.example.snookerapp.SharedPreference;
import com.example.snookerapp.activity.MatchesActivity;
import com.example.snookerapp.model.Player;
import com.squareup.picasso.Picasso;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

public class PlayerAdapter extends RecyclerView.Adapter<PlayerAdapter.ViewHolder> implements Filterable
{
    private final LayoutInflater inflater;
    private List<Player> allPlayersList;
    private List<Player> playerList;
    private SharedPreference sharedPreference;
    private OnRefreshListener refreshListener;

    public PlayerAdapter(Context context)
    {
        inflater = LayoutInflater.from(context);
        sharedPreference = new SharedPreference();
        refreshListener = (OnRefreshListener) context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = inflater.inflate(R.layout.view_element_player, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        Player current = playerList.get(position);

        if(checkFavoriteItem(current))
            holder.favButton.setColorFilter(ContextCompat.getColor(inflater.getContext(), R.color.colorYellowBall));
        else holder.favButton.clearColorFilter();

        String birthDate = parseDate(current.getBirthDate());
        String name = current.getFirstName() + " " + current.getLastName();
        String turnedPro = Integer.toString(current.getTurnedPro());

        holder.name.setText(name);
        holder.nationality.setText(current.getCountry());
        holder.turnedPro.setText(turnedPro);
        holder.birthDate.setText(birthDate);

        String imageURL = current.getImageURL();

        if(!imageURL.isEmpty())
        {
            Picasso.get().load(imageURL).centerCrop().fit().into(holder.image);
        }
        else holder.image.setImageResource(R.drawable.person_avatar);

        holder.frameLayout.setOnClickListener(view ->
        {
            Intent intent = new Intent(inflater.getContext(), MatchesActivity.class);
            intent.putExtra("PlayerID", current.getId());
            inflater.getContext().startActivity(intent);
        });

        holder.favButton.setOnClickListener(view ->
        {
            if(holder.favButton.getColorFilter() != null)
            {
                holder.favButton.clearColorFilter();
                sharedPreference.removeFavorite(inflater.getContext(), current);
                //Toast.makeText(inflater.getContext(), "Remove player from favorites", Toast.LENGTH_SHORT).show();
                refreshListener.onRefresh();
            }
            else
            {
                holder.favButton.setColorFilter(ContextCompat.getColor(inflater.getContext(), R.color.colorYellowBall));
                sharedPreference.addFavorite(inflater.getContext(), current);
                //Toast.makeText(inflater.getContext(), "Add player to favorites ", Toast.LENGTH_SHORT).show();
                refreshListener.onRefresh();

            }
        });
    }

    public void setAllPlayersList(List<Player> allPlayersList)
    {
        this.allPlayersList = allPlayersList;
        this.playerList = allPlayersList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount()
    {
        if (playerList != null)
            return playerList.size();
        else return 0;
    }

    private String parseDate(@NonNull String date)
    {
        final DateTimeFormatter OLD_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        final DateTimeFormatter NEW_FORMATTER = DateTimeFormatter.ofPattern("dd MMM yyyy");

        try
        {
            LocalDate localDate = LocalDate.parse(date, OLD_FORMATTER);
            return localDate.format(NEW_FORMATTER);
        }
        catch (DateTimeParseException exception)
        {
            return inflater.getContext().getResources().getString(R.string.unknown_date);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Filter getFilter()
    {
        return new Filter()
        {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence)
            {
                String charString = charSequence.toString();
                if (charString.isEmpty())
                {
                    playerList = allPlayersList;
                }
                else
                {
                    List<Player> filteredList = new ArrayList<>();
                    for (Player row : allPlayersList)
                    {
                        if (row.getFirstName().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getLastName().toLowerCase().contains(charString.toLowerCase()))
                        {
                            filteredList.add(row);
                        }
                    }
                    playerList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = playerList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults)
            {
                playerList = (List<Player>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    private boolean checkFavoriteItem(Player checkPlayer)
    {
        List<Player> favorites = sharedPreference.getFavorites(inflater.getContext());

        if (favorites != null)
            for (Player player : favorites)
                if (player.equals(checkPlayer))
                    return true;

        return false;
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView name, birthDate, turnedPro, nationality;
        private ImageView image;
        private FrameLayout frameLayout;
        private ImageView favButton;

        private ViewHolder(View v)
        {
            super(v);
            name =  v.findViewById(R.id.player_name);
            birthDate = v.findViewById(R.id.player_birthdate);
            turnedPro = v.findViewById(R.id.player_turned_pro);
            nationality = v.findViewById(R.id.player_nationality);

            image = v.findViewById(R.id.player_image);
            frameLayout = v.findViewById(R.id.player_view_frame_layout);
            favButton = v.findViewById(R.id.player_favorite_button);
        }
    }
}


