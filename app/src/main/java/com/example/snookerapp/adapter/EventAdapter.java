package com.example.snookerapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.snookerapp.R;
import com.example.snookerapp.activity.MatchesActivity;
import com.example.snookerapp.model.Event;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder>
{
    private final LayoutInflater inflater;
    private List<Event> eventList;

    public EventAdapter(Context context)
    {
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = inflater.inflate(R.layout.view_element_event, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        Event current = eventList.get(position);

        String startDate = parseDate(current.getStartDate());
        String endDate = parseDate(current.getEndDate());
        String cityAndCountry = current.getCity() + ", " + current.getCountry();

        holder.name.setText(current.getName());
        holder.city.setText(cityAndCountry);
        holder.venue.setText(current.getVenue());
        holder.type.setText(current.getType());
        holder.startDate.setText(startDate);
        holder.endDate.setText(endDate);

        holder.frameLayout.setOnClickListener(view ->
        {
            Intent intent = new Intent(inflater.getContext(), MatchesActivity.class);
            intent.putExtra("EventID", current.getId());
            inflater.getContext().startActivity(intent);
        });
    }

    public void setEventList(List<Event> eventList)
    {
        this.eventList = eventList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount()
    {
        if (eventList != null)
            return eventList.size();
        else return 0;
    }

    public void clearData()
    {
        int size = this.eventList.size();
        eventList.clear();
        notifyItemRangeRemoved(0, size);
    }

    private String parseDate(@NonNull String date)
    {
        final DateTimeFormatter OLD_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        final DateTimeFormatter NEW_FORMATTER = DateTimeFormatter.ofPattern("dd MMM yyyy");

        try
        {
            LocalDate localDate = LocalDate.parse(date, OLD_FORMATTER);
            return localDate.format(NEW_FORMATTER);
        }
        catch (DateTimeParseException exception)
        {
            return inflater.getContext().getResources().getString(R.string.unknown_date);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView name, startDate, endDate, city, venue, type;
        private FrameLayout frameLayout;

        private ViewHolder(View view)
        {
            super(view);
            name =  view.findViewById(R.id.event_name);
            startDate = view.findViewById(R.id.event_start_date);
            endDate = view.findViewById(R.id.event_end_date);
            city = view.findViewById(R.id.event_city_and_country);
            venue = view.findViewById(R.id.event_venue);
            type = view.findViewById(R.id.event_type);
            frameLayout = view.findViewById(R.id.event_view_frame_layout);
        }
    }
}
