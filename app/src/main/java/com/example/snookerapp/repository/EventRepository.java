package com.example.snookerapp.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.snookerapp.HTTPHandler;
import com.example.snookerapp.database.SnookerDatabase;
import com.example.snookerapp.database.dao.CountryDAO;
import com.example.snookerapp.database.dao.EventDAO;
import com.example.snookerapp.database.dao.EventRoundsDAO;
import com.example.snookerapp.database.dao.EventTypeDAO;
import com.example.snookerapp.database.dao.SeasonsDAO;
import com.example.snookerapp.database.entity.EventEntity;
import com.example.snookerapp.database.entity.EventRoundsEntity;
import com.example.snookerapp.model.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class EventRepository
{
    private EventDAO eventDAO;
    private SeasonsDAO seasonsDAO;
    private CountryDAO countryDAO;
    private EventTypeDAO eventTypeDAO;
    private EventRoundsDAO eventRoundsDAO;
    private MatchRepository matchRepository;

    public EventRepository(Application application)
    {
        SnookerDatabase database = SnookerDatabase.getDatabase(application);
        this.countryDAO = database.countryDAO();
        this.seasonsDAO = database.seasonsDAO();
        this.eventDAO = database.eventDAO();
        this.eventTypeDAO = database.eventTypeDAO();
        this.eventRoundsDAO = database.eventRoundsDAO();
        matchRepository = new MatchRepository(application);
    }

    public LiveData<List<Event>> getAllEvents()
    {
        return eventDAO.getAllEvents();
    }

    public LiveData<List<Event>> getRankingEvents()
    {
        return eventDAO.getRankingEvents();
    }

    public LiveData<List<Event>> getInvitationalEvents()
    {
        return eventDAO.getInvitationalEvents();
    }

    public LiveData<List<Event>> getQualifyingEvents()
    {
        return eventDAO.getQualifyingEvents();
    }

    public void insertEvents()
    {
        try
        {
            HTTPHandler eventHandler = new HTTPHandler();
            String eventURL = "http://api.snooker.org/?t=5&s=2019";
            String eventsJsonString = eventHandler.makeServiceCall(eventURL);

            if (eventsJsonString != null)
            {
                JSONArray eventsArray = new JSONArray(eventsJsonString);

                int seasonID = seasonsDAO.getSeasonID(2019);

                for (int i = 0; i < eventsArray.length(); i++)
                {
                    JSONObject eventObject = eventsArray.getJSONObject(i);

                    String type = eventObject.getString("Type");
                    String rankingType = eventObject.getString("RankingType");
                    String name = eventObject.getString("Name");


                    if(!eventObject.getBoolean("Team"))
                    {
                        if((type.equals("Ranking") && rankingType.equals("WR")) ||
                                ((type.equals("Invitational") && ((rankingType.equals("") || rankingType.equals("unknown") || rankingType.equals("Unknown") )) && !name.contains("Championship League")) ||
                                            (type.equals("Qualifying") && rankingType.equals("WR"))))
                        {
                            EventEntity event = new EventEntity();

                            String country = eventObject.getString("Country");
                            int countryID = countryDAO.getIdByName(country);
                            int eventTypeID = eventTypeDAO.getIdByName(type);
                            int eventID = eventObject.getInt("ID");

                            event.setID(eventID);
                            event.setName(eventObject.getString("Name"));
                            event.setStartDate(eventObject.getString("StartDate"));
                            event.setEndDate(eventObject.getString("EndDate"));
                            event.setCity(eventObject.getString("City"));
                            event.setVenue(eventObject.getString("Venue"));
                            event.setSeasonID(seasonID);
                            event.setCountryID(countryID);
                            event.setEventTypeID(eventTypeID);

                            eventDAO.insert(event);

                            if (eventObject.getBoolean("AllRoundsAdded"))
                            {
                                insertEventRounds(eventID);
                                matchRepository.insertMatches(eventID, type);
                            }
                        }
                    }
                }
            }

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void insertEventRounds(int eventID)
    {
        try
        {
            HTTPHandler eventRoundsHandler = new HTTPHandler();
            String eventRoundsURL = "http://api.snooker.org/?t=12&e=" + eventID;
            String eventRoundsJsonString = eventRoundsHandler.makeServiceCall(eventRoundsURL);

            if(eventRoundsJsonString != null)
            {
                JSONArray eventsRoundsArray = new JSONArray(eventRoundsJsonString);

                for (int i = 0; i < eventsRoundsArray.length(); i++)
                {
                    JSONObject eventRoundsObject = eventsRoundsArray.getJSONObject(i);

                    EventRoundsEntity eventRounds = new EventRoundsEntity();

                    eventRounds.setEventID(eventID);
                    eventRounds.setRoundID(eventRoundsObject.getInt("Round"));
                    eventRounds.setDistance(eventRoundsObject.getInt("Distance"));
                    eventRounds.setMoney(eventRoundsObject.getInt("Money"));

                    eventRoundsDAO.insert(eventRounds);
                }
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }
}
