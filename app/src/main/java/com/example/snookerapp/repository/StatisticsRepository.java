package com.example.snookerapp.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.snookerapp.database.SnookerDatabase;
import com.example.snookerapp.database.dao.PlayerPerformanceDAO;
import com.example.snookerapp.database.entity.PlayerPerformanceEntity;
import com.example.snookerapp.model.PlayerPerformance;

import java.util.List;

public class StatisticsRepository
{
    private PlayerPerformanceDAO playerPerformanceDAO;

    public StatisticsRepository(Application application)
    {
        SnookerDatabase database = SnookerDatabase.getDatabase(application);
        this.playerPerformanceDAO = database.playerPerformanceDAO();
    }

    public LiveData<List<PlayerPerformance>> getMatchStatistics()
    {
        return playerPerformanceDAO.getMatchStatistics();
    }

    public LiveData<List<PlayerPerformance>> getStatisticsOfPlayersWithMostWinningEvents()
    {
        return playerPerformanceDAO.getStatisticsOfPlayersWithMostWinningEvents();
    }

    public LiveData<List<PlayerPerformance>> getStatisticsOfPlayersWithMostWinningMatches()
    {
        return playerPerformanceDAO.getStatisticsOfPlayersWithMostWinningMatches();
    }

    public LiveData<List<PlayerPerformance>> getStatisticsOfPlayersWithMostLosingMatches()
    {
        return playerPerformanceDAO.getStatisticsOfPlayersWithMostLosingMatches();
    }

    public LiveData<List<PlayerPerformance>> etStatisticsOfCountriesWithMostWinningMatches()
    {
        return playerPerformanceDAO.getStatisticsOfCountriesWithMostWinningMatches();
    }

    public void updateMatchesPlayed(int playerID)
    {
        int matchesPlayed = playerPerformanceDAO.getMatchesPlayedByPlayerID(playerID) + 1;
        playerPerformanceDAO.updateMatchesPlayed(playerID, matchesPlayed);
    }

    public void updateWinningMatches(int playerID)
    {
        int winningMatches = playerPerformanceDAO.getWinningMatchesByPlayerID(playerID) + 1;
        playerPerformanceDAO.updateWinningMatches(playerID, winningMatches);
    }

    public void updateWinningEvent(int playerID)
    {
        int winningEvents = playerPerformanceDAO.getWinningEventsByPlayerID(playerID) + 1;
        playerPerformanceDAO.updateWinningEvents(playerID, winningEvents);
    }

    public void insertPlayerPerformance(int playerID, int seasonID, boolean proStatus)
    {
        PlayerPerformanceEntity performance = new PlayerPerformanceEntity(playerID, seasonID, proStatus);
        playerPerformanceDAO.insert(performance);
    }
}
