package com.example.snookerapp.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.snookerapp.HTTPHandler;
import com.example.snookerapp.database.SnookerDatabase;
import com.example.snookerapp.database.dao.EventDAO;
import com.example.snookerapp.database.dao.MatchDAO;
import com.example.snookerapp.database.dao.PlayerDAO;
import com.example.snookerapp.database.entity.MatchEntity;
import com.example.snookerapp.model.Match;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class MatchRepository
{
    private MatchDAO matchDAO;
    private PlayerDAO playerDAO;
    private EventDAO eventDAO;
    private final PlayerRepository playerRepository;
    private final StatisticsRepository playerPerformanceRepository;

    public MatchRepository(Application application)
    {
        SnookerDatabase database = SnookerDatabase.getDatabase(application);
        this.matchDAO = database.matchDAO();
        this.playerDAO = database.playerDAO();
        this.eventDAO = database.eventDAO();
        this.playerRepository = new PlayerRepository(application);
        this.playerPerformanceRepository = new StatisticsRepository(application);
    }

    public LiveData<List<Match>> getAllMatchesByPlayerId(int playerID)
    {
        return matchDAO.getAllMatchesByPlayerId(playerID);
    }

    public LiveData<List<Match>> getAllMatchesByEventId(int eventID)
    {
        return matchDAO.getAllMatchesByEventId(eventID);
    }

    public void insertMatches(int eventID, String type)
    {
        boolean isRanking;

        if(eventID != 0)
        {
            try
            {
                isRanking = type.equals("Ranking");

                HTTPHandler matchesHandler = new HTTPHandler();
                String matchesURL = "http://api.snooker.org/?t=6&e=" + eventID;
                String matchesJsonString = matchesHandler.makeServiceCall(matchesURL);

                if (matchesJsonString != null)
                {
                    JSONArray matchesArray = new JSONArray(matchesJsonString);

                    for (int i = 0; i < matchesArray.length(); i++) {
                        JSONObject matchesObject = matchesArray.getJSONObject(i);

                        int winnerID = matchesObject.getInt("WinnerID");

                        if (winnerID != 0)
                        {
                            MatchEntity match = new MatchEntity();

                            int matchID = matchesObject.getInt("WorldSnookerID");

                            if (matchID == 0)
                                matchID = matchesObject.getInt("ID");

                            int player1ID = matchesObject.getInt("Player1ID");
                            int player2ID = matchesObject.getInt("Player2ID");
                            int roundID = matchesObject.getInt("Round");

                            if (player1ID != playerDAO.checkPlayerExist(player1ID))
                                playerRepository.insertPlayer(player1ID);

                            if (player2ID != playerDAO.checkPlayerExist(player2ID))
                                playerRepository.insertPlayer(player2ID);

                            if (matchID != matchDAO.getMatchByID(matchID) && isRanking)
                            {
                                playerPerformanceRepository.updateMatchesPlayed(player1ID);
                                playerPerformanceRepository.updateMatchesPlayed(player2ID);
                                playerPerformanceRepository.updateWinningMatches(winnerID);

                                if (roundID == 15)
                                    playerPerformanceRepository.updateWinningEvent(winnerID);
                            }

                            match.setPlayer1ID(player1ID);
                            match.setPlayer2ID(player2ID);
                            match.setWinnerID(winnerID);
                            match.setID(matchID);
                            match.setPlayer1score(matchesObject.getInt("Score1"));
                            match.setPlayer2score(matchesObject.getInt("Score2"));
                            match.setDate(matchesObject.getString("StartDate"));
                            match.setEventID(eventID);
                            match.setRoundID(roundID);

                            matchDAO.insert(match);
                        }
                    }
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }
}
