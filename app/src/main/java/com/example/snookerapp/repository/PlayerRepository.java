package com.example.snookerapp.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.snookerapp.HTTPHandler;
import com.example.snookerapp.database.SnookerDatabase;
import com.example.snookerapp.database.dao.CountryDAO;
import com.example.snookerapp.database.dao.PlayerDAO;
import com.example.snookerapp.database.dao.SeasonsDAO;
import com.example.snookerapp.database.entity.PlayerEntity;
import com.example.snookerapp.model.Player;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class PlayerRepository
{
    private PlayerDAO playerDAO;
    private SeasonsDAO seasonsDAO;
    private CountryDAO countryDAO;
    private StatisticsRepository playerPerformanceRepository;

    public PlayerRepository(Application application)
    {
        SnookerDatabase database = SnookerDatabase.getDatabase(application);
        this.playerDAO = database.playerDAO();
        this.countryDAO = database.countryDAO();
        this.seasonsDAO = database.seasonsDAO();
        this.playerPerformanceRepository = new StatisticsRepository(application);
    }

    public LiveData<List<Player>> getAllPlayersSortById()
    {
        return playerDAO.getAllPlayersSortById();
    }

    public LiveData<List<Player>> getAllPlayersSortByFirstName()
    {
        return playerDAO.getAllPlayersSortByFirstName();
    }

    public LiveData<List<Player>> getAllPlayersSortByLastName()
    {
        return playerDAO.getAllPlayersSortByLastName();
    }

    public LiveData<List<Player>> getAllPlayersSortByCountry()
    {
        return playerDAO.getAllPlayersSortByCountry();
    }

    public void insertProPlayers()
    {
        try
        {
            HTTPHandler playersHandler = new HTTPHandler();
            String playersURL = "http://api.snooker.org/?t=10&st=p&s=2019";
            String playersJsonString = playersHandler.makeServiceCall(playersURL);

            int seasonID = seasonsDAO.getSeasonID(2019);

            if(playersJsonString != null)
            {
                JSONArray playersArray = new JSONArray(playersJsonString);

                for (int i = 0; i < playersArray.length(); i++)
                {
                    PlayerEntity player = new PlayerEntity();

                    JSONObject playersObject = playersArray.getJSONObject(i);

                    String country = playersObject.getString("Nationality");
                    int countryID = countryDAO.getIdByName(country);
                    int playerID = playersObject.getInt("ID");

                    player.setID(playerID);
                    player.setFirstName(playersObject.getString("FirstName"));
                    player.setLastName(playersObject.getString("LastName"));
                    player.setTurnedPro(playersObject.getInt("FirstSeasonAsPro"));
                    player.setBirthDate(playersObject.getString("Born"));
                    player.setImageURL(playersObject.getString("Photo"));
                    player.setCountryID(countryID);

                    playerDAO.insert(player);
                    playerPerformanceRepository.insertPlayerPerformance(playerID, seasonID, true);
                }
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public void insertPlayer(int playerID)
    {
        int seasonID = seasonsDAO.getSeasonID(2019);

        try
        {
            HTTPHandler playerHandler = new HTTPHandler();
            String playerURL = "http://api.snooker.org/?p=" + playerID;
            String playerJsonString = playerHandler.makeServiceCall(playerURL);

            if(playerJsonString != null)
            {
                JSONArray playerArray = new JSONArray(playerJsonString);
                JSONObject playerObject = playerArray.getJSONObject(0);

                PlayerEntity player = new PlayerEntity();

                player.setID(playerID);
                player.setFirstName(playerObject.getString("FirstName"));
                player.setLastName(playerObject.getString("LastName"));
                player.setCountryID(countryDAO.getIdByName("TBD"));

                playerDAO.insert(player);
                playerPerformanceRepository.insertPlayerPerformance(playerID, seasonID, false);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }
}
