package com.example.snookerapp.database.entity;

import androidx.annotation.NonNull;
import androidx.room.*;

@Entity (tableName = "Countries")
public class CountryEntity
{
    @ColumnInfo (name = "ID")
    @PrimaryKey (autoGenerate = true)
    private int ID;

    @ColumnInfo (name = "name")
    @NonNull
    private String name;

    public CountryEntity(@NonNull String name)
    {
        this.name = name;
    }

    public int getID()
    {
        return ID;
    }

    @NonNull
    public String getName()
    {
        return name;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }

}
