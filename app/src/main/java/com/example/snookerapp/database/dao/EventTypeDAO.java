package com.example.snookerapp.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.snookerapp.database.entity.EventTypeEntity;

import java.util.List;

@Dao
public interface EventTypeDAO
{
    @Query("SELECT ID FROM EventTypes WHERE name = :eventTypeName")
    int getIdByName(String eventTypeName);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(List<EventTypeEntity> eventsList);
}
