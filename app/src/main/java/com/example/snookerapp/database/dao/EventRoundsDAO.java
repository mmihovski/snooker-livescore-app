package com.example.snookerapp.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.snookerapp.database.entity.EventRoundsEntity;

@Dao
public interface EventRoundsDAO
{
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(EventRoundsEntity eventRounds);

    @Query("SELECT distance FROM EventRounds WHERE event_ID = :eventID AND round_ID = :roundID")
    int getDistance(int eventID, int roundID);
}
