package com.example.snookerapp.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.snookerapp.database.dao.*;
import com.example.snookerapp.database.entity.*;

@Database(entities = {CountryEntity.class, PlayerEntity.class, EventTypeEntity.class, RoundEntity.class,
        SeasonEntity.class, PlayerPerformanceEntity.class, EventEntity.class, EventRoundsEntity.class, MatchEntity.class},
        version = 25, exportSchema = false)
public abstract class SnookerDatabase extends RoomDatabase
{
    private static SnookerDatabase INSTANCE;

    public abstract CountryDAO countryDAO();
    public abstract PlayerDAO playerDAO();
    public abstract EventTypeDAO eventTypeDAO();
    public abstract RoundDAO roundDAO();
    public abstract SeasonsDAO seasonsDAO();
    public abstract PlayerPerformanceDAO playerPerformanceDAO();
    public abstract EventDAO eventDAO();
    public abstract EventRoundsDAO eventRoundsDAO();
    public abstract MatchDAO matchDAO();

    public static synchronized SnookerDatabase getDatabase(final Context context)
    {
        if(INSTANCE == null)
        {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), SnookerDatabase.class, "SnookerDatabase").
                    fallbackToDestructiveMigration().build();
        }
        return INSTANCE;
    }
}


