package com.example.snookerapp.database.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity (tableName = "EventTypes")
public class EventTypeEntity
{
    @ColumnInfo(name = "ID")
    @PrimaryKey(autoGenerate = true)
    private int ID;

    @ColumnInfo (name = "name")
    @NonNull
    private String name;

    public EventTypeEntity(@NonNull String name)
    {
        this.name = name;
    }

    public int getID()
    {
        return ID;
    }

    @NonNull
    public String getName()
    {
        return name;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }
}
