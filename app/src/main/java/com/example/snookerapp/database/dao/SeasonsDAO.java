package com.example.snookerapp.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.snookerapp.database.entity.SeasonEntity;

import java.util.List;

@Dao
public interface SeasonsDAO
{
    @Query("SELECT s.ID FROM Seasons s WHERE start_year = :startYear")
    int getSeasonID(int startYear);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(List<SeasonEntity> seasonsList);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(SeasonEntity season);
}
