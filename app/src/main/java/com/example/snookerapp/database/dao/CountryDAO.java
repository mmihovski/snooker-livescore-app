package com.example.snookerapp.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.snookerapp.database.entity.CountryEntity;

import java.util.List;

@Dao
public interface CountryDAO
{
    @Query("SELECT ID FROM Countries WHERE name = :countryName")
    int getIdByName(String countryName);

    @Insert (onConflict = OnConflictStrategy.IGNORE)
    void insert(List<CountryEntity> countriesList);

    @Insert (onConflict = OnConflictStrategy.IGNORE)
    void insert(CountryEntity country);

}
