package com.example.snookerapp.database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity (tableName = "Seasons")
public class SeasonEntity
{
    @ColumnInfo(name = "ID")
    @PrimaryKey(autoGenerate = true)
    private int ID;

    @ColumnInfo (name = "start_year")
    private int startYear;

    @ColumnInfo (name = "end_year")
    private int endYear;

    public SeasonEntity(int startYear, int endYear)
    {
        this.startYear = startYear;
        this.endYear = endYear;
    }

    public int getID()
    {
        return ID;
    }

    public int getEndYear()
    {
        return endYear;
    }

    public int getStartYear()
    {
        return startYear;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }
}
