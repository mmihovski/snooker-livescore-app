package com.example.snookerapp.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.snookerapp.database.entity.RoundEntity;

import java.util.List;

@Dao
public interface RoundDAO
{
    @Query("SELECT name FROM Rounds WHERE ID = :id")
    String getRoundNameByID(int id);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(List<RoundEntity> roundsList);
}
