package com.example.snookerapp.database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "Players", indices = {@Index("country_ID")},
        foreignKeys = @ForeignKey(entity = CountryEntity.class, parentColumns = "ID", childColumns = "country_ID"))
public class PlayerEntity
{
    @ColumnInfo(name = "ID")
    @PrimaryKey
    private int ID;

    @ColumnInfo(name = "first_name")
    private String firstName;

    @ColumnInfo(name = "last_name")
    private String lastName;

    @ColumnInfo(name = "birthdate")
    private String birthDate;

    @ColumnInfo(name = "turned_pro")
    private int turnedPro;

    @ColumnInfo(name = "country_ID")
    private int countryID;

    @ColumnInfo(name = "image_URL")
    private String imageURL;

    public int getID()
    {
        return ID;
    }

    public int getCountryID()
    {
        return countryID;
    }

    public int getTurnedPro()
    {
        return turnedPro;
    }

    public String getBirthDate()
    {
        return birthDate;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public String getImageURL()
    {
        return imageURL;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setTurnedPro(int turnedPro) {
        this.turnedPro = turnedPro;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setCountryID(int countryID) {
        this.countryID = countryID;
    }

    public void setImageURL(String imageURL)
    {
        this.imageURL = imageURL;
    }
}
