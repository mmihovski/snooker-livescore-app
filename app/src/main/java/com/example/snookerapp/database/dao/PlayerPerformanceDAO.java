package com.example.snookerapp.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.RoomWarnings;

import com.example.snookerapp.database.entity.PlayerPerformanceEntity;
import com.example.snookerapp.model.PlayerPerformance;

import java.util.List;

@SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
@Dao
public interface PlayerPerformanceDAO
{
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(PlayerPerformanceEntity playerPerformance);

    @Query("SELECT pp.matches_played FROM PlayersPerformance pp WHERE pp.player_ID = :playerID")
    int getMatchesPlayedByPlayerID(int playerID);

    @Query("SELECT pp.winning_matches FROM PlayersPerformance pp WHERE pp.player_ID = :playerID")
    int getWinningMatchesByPlayerID(int playerID);

    @Query("SELECT pp.winning_events FROM PlayersPerformance pp WHERE pp.player_ID = :playerID")
    int getWinningEventsByPlayerID(int playerID);

    @Query("UPDATE PlayersPerformance SET matches_played =:matchesPlayed WHERE player_ID = :playerID")
    void updateMatchesPlayed(int playerID, int matchesPlayed);

    @Query("UPDATE PlayersPerformance SET winning_matches =:winningMatches WHERE player_ID = :playerID")
    void updateWinningMatches(int playerID, int winningMatches);

    @Query("UPDATE PlayersPerformance SET winning_events =:winningEvents WHERE player_ID = :playerID")
    void updateWinningEvents(int playerID, int winningEvents);

    @Query("SELECT p.first_name || ' ' || p.last_name as name, " +
            "(pp.winning_matches * 100 / pp.matches_played)  || ' %   ' || " +
            "pp.winning_matches || ' / ' || pp.matches_played as value " +
            "FROM PlayersPerformance pp " +
            "JOIN Players p ON pp.player_ID = p.ID " +
            "WHERE pp.pro_status = 1 " +
            "ORDER BY (pp.winning_matches * 100 / pp.matches_played) DESC")
    LiveData<List<PlayerPerformance>> getMatchStatistics();

    @Query("SELECT p.first_name || ' ' || p.last_name as name, " +
            "pp.winning_matches as value " +
            "FROM PlayersPerformance pp " +
            "JOIN Players p ON pp.player_ID = p.ID " +
            "WHERE pp.pro_status = 1 AND pp.winning_matches > 0 " +
            "ORDER BY pp.winning_matches DESC")
    LiveData<List<PlayerPerformance>> getStatisticsOfPlayersWithMostWinningMatches();

    @Query("SELECT p.first_name || ' ' || p.last_name as name, " +
            "pp.matches_played - pp.winning_matches as value " +
            "FROM PlayersPerformance pp " +
            "JOIN Players p ON pp.player_ID = p.ID " +
            "WHERE pp.pro_status = 1 AND pp.matches_played - pp.winning_matches > 0 " +
            "ORDER BY pp.matches_played - pp.winning_matches DESC")
    LiveData<List<PlayerPerformance>> getStatisticsOfPlayersWithMostLosingMatches();

    @Query("SELECT p.first_name || ' ' || p.last_name as name, " +
            "pp.winning_events as value " +
            "FROM PlayersPerformance pp " +
            "JOIN Players p ON pp.player_ID = p.ID " +
            "WHERE pp.pro_status = 1 AND pp.winning_events > 0 " +
            "ORDER BY pp.winning_events DESC")
    LiveData<List<PlayerPerformance>> getStatisticsOfPlayersWithMostWinningEvents();

    @Query("SELECT c.name as name, " +
            "SUM(pp.winning_matches) as value " +
            "FROM PlayersPerformance pp " +
            "JOIN Players p ON pp.player_ID = p.ID " +
            "JOIN Countries c ON p.country_ID = c.ID " +
            "WHERE pp.pro_status = 1 " +
            "GROUP BY c.name " +
            "HAVING SUM(pp.winning_matches) > 0 " +
            "ORDER BY SUM(pp.winning_matches) DESC")
    LiveData<List<PlayerPerformance>> getStatisticsOfCountriesWithMostWinningMatches();

}
