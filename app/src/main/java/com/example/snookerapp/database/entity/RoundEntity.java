package com.example.snookerapp.database.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity (tableName = "Rounds")
public class RoundEntity
{
    @ColumnInfo(name = "ID")
    @PrimaryKey
    private int ID;

    @ColumnInfo (name = "name")
    @NonNull
    private String name;

    public RoundEntity(int ID, @NonNull String name)
    {
        this.ID = ID;
        this.name = name;
    }

    public int getID()
    {
        return ID;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }

    @NonNull
    public String getName()
    {
        return name;
    }

    public void setName(@NonNull String name)
    {
        this.name = name;
    }
}
