package com.example.snookerapp.database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;

import static java.sql.Types.BOOLEAN;

@Entity (tableName = "PlayersPerformance",
        primaryKeys = {"player_ID", "season_ID"},
        foreignKeys =
                {
                        @ForeignKey(entity = PlayerEntity.class, parentColumns = "ID", childColumns = "player_ID"),
                        @ForeignKey(entity = SeasonEntity.class, parentColumns = "ID", childColumns = "season_ID")
                })
public class PlayerPerformanceEntity
{
    @ColumnInfo(name = "player_ID", index = true)
    private int playerID;

    @ColumnInfo(name = "season_ID", index = true)
    private int seasonID;

    @ColumnInfo(name = "pro_status", typeAffinity = BOOLEAN)
    private boolean proStatus;

    @ColumnInfo(name = "winning_events")
    private int winningEvents;

    @ColumnInfo(name = "matches_played")
    private int matchesPlayed;

    @ColumnInfo(name = "winning_matches")
    private int winningMatches;

    public PlayerPerformanceEntity(int playerID, int seasonID, boolean proStatus)
    {
        this.playerID = playerID;
        this.seasonID = seasonID;
        this.proStatus = proStatus;
    }

    public int getPlayerID() {
        return playerID;
    }

    public void setPlayerID(int playerID) {
        this.playerID = playerID;
    }

    public int getSeasonID() {
        return seasonID;
    }

    public void setSeasonID(int seasonID) {
        this.seasonID = seasonID;
    }

    public boolean isProStatus() {
        return proStatus;
    }

    public void setProStatus(boolean proStatus) {
        this.proStatus = proStatus;
    }

    public int getWinningEvents() {
        return winningEvents;
    }

    public void setWinningEvents(int winningEvents) {
        this.winningEvents = winningEvents;
    }

    public int getMatchesPlayed() {
        return matchesPlayed;
    }

    public void setMatchesPlayed(int matchesPlayed) {
        this.matchesPlayed = matchesPlayed;
    }

    public int getWinningMatches() {
        return winningMatches;
    }

    public void setWinningMatches(int winningMatches) {
        this.winningMatches = winningMatches;
    }
}
