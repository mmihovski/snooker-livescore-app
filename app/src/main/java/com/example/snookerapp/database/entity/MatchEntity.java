package com.example.snookerapp.database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

@Entity (tableName = "Matches", indices = {@Index (value = {"event_ID","round_ID"})},
        primaryKeys = "ID",
        foreignKeys =
                {
                        @ForeignKey(entity = PlayerEntity.class, parentColumns = "ID", childColumns = "player1_ID"),
                        @ForeignKey(entity = PlayerEntity.class, parentColumns = "ID", childColumns = "player2_ID"),
                        @ForeignKey(entity = PlayerEntity.class, parentColumns = "ID", childColumns = "winner_ID"),
                        @ForeignKey(entity = EventEntity.class, parentColumns = "ID", childColumns = "event_ID"),
                        @ForeignKey(entity = RoundEntity.class, parentColumns = "ID", childColumns = "round_ID"),
                        @ForeignKey(entity = EventRoundsEntity.class,
                                parentColumns = {"event_ID", "round_ID"}, childColumns = {"event_ID", "round_ID"})
                })
public class MatchEntity
{
    @ColumnInfo(name = "ID")
    private int ID;

    @ColumnInfo(name = "player1_ID", index = true)
    private int player1ID;

    @ColumnInfo(name = "player2_ID", index = true)
    private int player2ID;

    @ColumnInfo(name =  "winner_ID", index = true)
    private int winnerID;

    @ColumnInfo(name = "player1_score")
    private int player1score;

    @ColumnInfo(name = "player2_score")
    private int player2score;

    @ColumnInfo(name = "event_ID", index = true)
    private int eventID;

    @ColumnInfo(name = "round_ID", index = true)
    private int roundID;

    @ColumnInfo(name = "date")
    private String date;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getPlayer1ID() {
        return player1ID;
    }

    public void setPlayer1ID(int player1ID) {
        this.player1ID = player1ID;
    }

    public int getPlayer2ID() {
        return player2ID;
    }

    public void setPlayer2ID(int player2ID) {
        this.player2ID = player2ID;
    }

    public int getWinnerID() {
        return winnerID;
    }

    public void setWinnerID(int winnerID) {
        this.winnerID = winnerID;
    }

    public int getPlayer1score() {
        return player1score;
    }

    public void setPlayer1score(int player1score) {
        this.player1score = player1score;
    }

    public int getPlayer2score() {
        return player2score;
    }

    public void setPlayer2score(int player2score) {
        this.player2score = player2score;
    }

    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public int getRoundID() {
        return roundID;
    }

    public void setRoundID(int roundID) {
        this.roundID = roundID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
