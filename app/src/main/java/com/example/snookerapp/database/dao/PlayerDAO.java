package com.example.snookerapp.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.RoomWarnings;

import com.example.snookerapp.database.entity.PlayerEntity;
import com.example.snookerapp.model.Player;

import java.util.List;

@SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
@Dao
public interface PlayerDAO
{
    @Query("SELECT p.*, c.name as country " +
            "FROM Players p " +
            "JOIN Countries c ON p.country_ID = c.ID " +
            "JOIN PlayersPerformance pp ON p.ID = pp.player_ID " +
            "WHERE pp.pro_status = 1 " +
            "ORDER BY p.last_name ASC")
    LiveData<List<Player>> getAllPlayersSortByLastName();

    @Query("SELECT p.*, c.name as country " +
            "FROM Players p " +
            "JOIN Countries c ON p.country_ID = c.ID " +
            "JOIN PlayersPerformance pp ON p.ID = pp.player_ID " +
            "WHERE pp.pro_status = 1 " +
            "ORDER BY p.first_name ASC")
    LiveData<List<Player>> getAllPlayersSortByFirstName();

    @Query("SELECT p.*, c.name as country " +
            "FROM Players p " +
            "JOIN Countries c ON p.country_ID = c.ID " +
            "JOIN PlayersPerformance pp ON p.ID = pp.player_ID " +
            "WHERE pp.pro_status = 1 " +
            "ORDER BY c.name ASC")
    LiveData<List<Player>> getAllPlayersSortByCountry();

    @Query("SELECT p.*, c.name as country " +
            "FROM Players p " +
            "JOIN Countries c ON p.country_ID = c.ID " +
            "JOIN PlayersPerformance pp ON p.ID = pp.player_ID " +
            "WHERE pp.pro_status = 1 ")
    LiveData<List<Player>> getAllPlayersSortById();

    @Query("SELECT ID FROM Players WHERE ID = :id")
    int checkPlayerExist(int id);

    @Query("SELECT first_name || ' ' || last_name FROM Players WHERE ID = :id")
    String getPlayerNameByID(int id);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(PlayerEntity player);
}
