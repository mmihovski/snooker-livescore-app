package com.example.snookerapp.database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;

@Entity (tableName = "Events",
        primaryKeys = "ID",
        foreignKeys =
        {
                @ForeignKey(entity = CountryEntity.class, parentColumns = "ID", childColumns = "country_ID"),
                @ForeignKey(entity = SeasonEntity.class, parentColumns = "ID", childColumns = "season_ID"),
                @ForeignKey(entity = EventTypeEntity.class, parentColumns = "ID", childColumns = "event_type_ID")
        })
public class EventEntity
{
    @ColumnInfo(name = "ID")
    private int ID;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "start_date")
    private String startDate;

    @ColumnInfo(name = "end_date")
    private String endDate;

    @ColumnInfo(name = "city")
    private String city;

    @ColumnInfo(name = "venue")
    private String venue;

    @ColumnInfo(name = "country_ID", index = true)
    private int countryID;

    @ColumnInfo(name = "season_ID", index = true)
    private int seasonID;

    @ColumnInfo(name = "event_type_ID", index = true)
    private int eventTypeID;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public int getCountryID() {
        return countryID;
    }

    public void setCountryID(int countryID) {
        this.countryID = countryID;
    }

    public int getSeasonID() {
        return seasonID;
    }

    public void setSeasonID(int seasonID) {
        this.seasonID = seasonID;
    }

    public int getEventTypeID() {
        return eventTypeID;
    }

    public void setEventTypeID(int eventTypeID) {
        this.eventTypeID = eventTypeID;
    }

}
