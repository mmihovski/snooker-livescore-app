package com.example.snookerapp.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.snookerapp.database.entity.MatchEntity;
import com.example.snookerapp.model.Match;

import java.util.List;

@Dao
public interface MatchDAO
{
    @Query("SELECT (p1.first_name || ' ' || p1.last_name) as player1, " +
            "(p2.first_name || ' ' || p2.last_name) as player2," +
            "(w.first_name || ' ' || w.last_name) as winner, " +
            "r.name as round, er.distance, e.name as event, " +
            "m.date, m.player1_score, m.player2_score " +
            "FROM Matches m " +
            "JOIN Rounds r ON r.ID = m.round_ID " +
            "JOIN Events e ON e.ID = m.event_ID " +
            "JOIN EventRounds er ON er.event_ID = m.event_ID AND er.round_ID = m.round_ID " +
            "JOIN Players p1 ON p1.ID = m.player1_ID " +
            "JOIN Players p2 ON p2.ID = m.player2_ID " +
            "JOIN Players w ON w.ID = m.winner_ID " +
            "WHERE m.player1_ID = :playerID OR m.player2_ID = :playerID " +
            "ORDER BY m.date DESC")
    LiveData<List<Match>> getAllMatchesByPlayerId(int playerID);

    @Query("SELECT (p1.first_name || ' ' || p1.last_name) as player1, " +
            "(p2.first_name || ' ' || p2.last_name) as player2, " +
            "(w.first_name || ' ' || w.last_name) as winner, " +
            "r.name as round, er.distance, e.name as event, " +
            "m.date, m.player1_score, m.player2_score " +
            "FROM Matches m " +
            "JOIN Rounds r ON r.ID = m.round_ID " +
            "JOIN Events e ON e.ID = m.event_ID " +
            "JOIN EventRounds er ON er.event_ID = m.event_ID AND er.round_ID = m.round_ID " +
            "JOIN Players p1 ON p1.ID = m.player1_ID " +
            "JOIN Players p2 ON p2.ID = m.player2_ID " +
            "JOIN Players w ON w.ID = m.winner_ID " +
            "WHERE m.event_ID = :eventID " +
            "ORDER BY m.date DESC")
    LiveData<List<Match>> getAllMatchesByEventId(int eventID);

    @Query("SELECT ID FROM Matches WHERE ID = :matchID")
    int getMatchByID(int matchID);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(MatchEntity match);
}
