package com.example.snookerapp.database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;

@Entity(tableName = "EventRounds",
        primaryKeys = {"event_ID", "round_ID"},
        foreignKeys =
                {
                        @ForeignKey(entity = EventEntity.class, parentColumns = "ID", childColumns = "event_ID"),
                        @ForeignKey(entity = RoundEntity.class, parentColumns = "ID", childColumns = "round_ID")
                })
public class EventRoundsEntity
{
    @ColumnInfo(name = "event_ID", index = true )
    private int eventID;

    @ColumnInfo(name = "round_ID", index = true)
    private int roundID;

    @ColumnInfo(name = "distance")
    private int distance;

    @ColumnInfo(name = "money")
    private int money;


    public int getEventID() {
        return eventID;
    }

    public int getRoundID() {
        return roundID;
    }

    public int getDistance() {
        return distance;
    }

    public int getMoney() {
        return money;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public void setRoundID(int roundID) {
        this.roundID = roundID;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    @Override
    public String toString()
    {
        return getEventID() + " " + getRoundID();
    }
}
