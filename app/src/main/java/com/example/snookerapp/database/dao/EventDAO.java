package com.example.snookerapp.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.RoomWarnings;

import com.example.snookerapp.database.entity.EventEntity;
import com.example.snookerapp.model.Event;

import java.util.List;

@SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
@Dao
public interface EventDAO
{
    @Query("SELECT e.*, c.name as country, et.name as type " +
            "FROM Events e " +
            "JOIN Countries c ON e.country_ID = c.ID " +
            "JOIN EventTypes et ON e.event_type_ID = et.ID " +
            "ORDER BY e.start_date ASC")
    LiveData<List<Event>> getAllEvents();

    @Query("SELECT e.*, c.name as country, et.name as type " +
            "FROM Events e " +
            "JOIN Countries c ON e.country_ID = c.ID " +
            "JOIN EventTypes et ON e.event_type_ID = et.ID " +
            "WHERE et.name = 'Ranking' " +
            "ORDER BY e.start_date ASC")
    LiveData<List<Event>> getRankingEvents();

    @Query("SELECT e.*, c.name as country, et.name as type " +
            "FROM Events e " +
            "JOIN Countries c ON e.country_ID = c.ID " +
            "JOIN EventTypes et ON e.event_type_ID = et.ID " +
            "WHERE et.name = 'Invitational' " +
            "ORDER BY e.start_date ASC")
    LiveData<List<Event>> getInvitationalEvents();

    @Query("SELECT e.*, c.name as country, et.name as type " +
            "FROM Events e " +
            "JOIN Countries c ON e.country_ID = c.ID " +
            "JOIN EventTypes et ON e.event_type_ID = et.ID " +
            "WHERE et.name = 'Qualifying' " +
            "ORDER BY e.start_date ASC")
    LiveData<List<Event>> getQualifyingEvents();

    @Query("SELECT name FROM Events WHERE ID = :id")
    String getEventNameByID(int id);

    @Query("SELECT e.ID, e.start_date, e.end_date " +
            "FROM Events e " +
            "ORDER BY e.start_date ASC")
    List<Event> getEventsDates();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(EventEntity event);
}
