package com.example.snookerapp.model;

public class Ranking
{
    private int position;
    private String playerName;
    private int points;

    public void setPlayerName(String playerName)
    {
        this.playerName = playerName;
    }

    public void setPoints(int points)
    {
        this.points = points;
    }

    public void setPosition(int position)
    {
        this.position = position;
    }

    public int getPosition()
    {
        return position;
    }

    public String getPlayerName()
    {
        return playerName;
    }

    public int getPoints()
    {
        return points;
    }
}
