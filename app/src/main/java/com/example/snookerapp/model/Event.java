package com.example.snookerapp.model;

import androidx.room.ColumnInfo;

public class Event
{
    @ColumnInfo(name = "ID")
    private int id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "start_date")
    private String startDate;

    @ColumnInfo(name = "end_date")
    private String endDate;

    @ColumnInfo(name = "country")
    private String country;

    @ColumnInfo(name = "city")
    private String city;

    @ColumnInfo(name = "venue")
    private String venue;

    @ColumnInfo(name = "type")
    private String type;

    public int getId() {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setStartDate(String startDate)
    {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate)
    {
        this.endDate = endDate;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public void setVenue(String venue)
    {
        this.venue = venue;
    }

    public void setType(String type)
    {
        this.type = type;
    }


    public String getName()
    {
        return name;
    }

    public String getStartDate()
    {
        return startDate;
    }

    public String getEndDate()
    {
        return endDate;
    }

    public String getCountry()
    {
        return country;
    }

    public String getCity()
    {
        return city;
    }

    public String getVenue()
    {
        return venue;
    }

    public String getType()
    {
        return type;
    }
}
