package com.example.snookerapp.model;

import androidx.room.ColumnInfo;
import androidx.room.Ignore;

public class Match
{
    @ColumnInfo(name = "event")
    private String event;

    @ColumnInfo(name = "round")
    private String round;

    @ColumnInfo(name = "player1")
    private String player1;

    @ColumnInfo(name = "player2")
    private String player2;

    @ColumnInfo(name = "winner")
    private String winner;

    @ColumnInfo(name = "player1_score")
    private int score1;

    @ColumnInfo(name = "player2_score")
    private int score2;

    @ColumnInfo(name = "distance")
    private int distance;

    @ColumnInfo(name = "date")
    private String date;

    @Ignore
    private boolean onBreak;

    public void setOnBreak(boolean onBreak)
    {
        this.onBreak = onBreak;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public void setDistance(int distance)
    {
        this.distance = distance;
    }

    public void setEvent(String event)
    {
        this.event = event;
    }

    public void setPlayer1(String player1)
    {
        this.player1 = player1;
    }

    public void setPlayer2(String player2)
    {
        this.player2 = player2;
    }

    public void setRound(String round)
    {
        this.round = round;
    }

    public void setScore1(int score1)
    {
        this.score1 = score1;
    }

    public void setScore2(int score2)
    {
        this.score2 = score2;
    }

    public void setWinner(String winner)
    {
        this.winner = winner;
    }


    public int getDistance()
    {
        return distance;
    }

    public int getScore1()
    {
        return score1;
    }

    public int getScore2()
    {
        return score2;
    }

    public String getDate()
    {
        return date;
    }

    public String getEvent()
    {
        return event;
    }

    public String getPlayer1()
    {
        return player1;
    }

    public String getPlayer2()
    {
        return player2;
    }

    public String getRound()
    {
        return round;
    }

    public String getWinner()
    {
        return winner;
    }

    public boolean isOnBreak()
    {
        return onBreak;
    }
}

