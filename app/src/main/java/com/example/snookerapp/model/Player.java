package com.example.snookerapp.model;

import androidx.room.ColumnInfo;

public class Player
{
    @ColumnInfo(name = "ID")
    private int id;

    @ColumnInfo(name = "first_name")
    private String firstName;

    @ColumnInfo(name = "last_name")
    private String lastName;

    @ColumnInfo(name = "birthdate")
    private String birthDate;

    @ColumnInfo(name = "turned_pro")
    private int turnedPro;

    @ColumnInfo(name = "country")
    private String country;

    @ColumnInfo(name = "image_URL")
    private String imageURL;

    public void setId(int id)
    {
        this.id = id;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public void setBirthDate(String birthDate)
    {
        this.birthDate = birthDate;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public void setTurnedPro(int turnedPro)
    {
        this.turnedPro = turnedPro;
    }

    public void setImageURL(String imageURL)
    {
        this.imageURL = imageURL;
    }


    public int getId()
    {
        return id;
    }

    public String getLastName()
    {
        return lastName;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getBirthDate()
    {
        return birthDate;
    }

    public int getTurnedPro()
    {
        return turnedPro;
    }

    public String getCountry()
    {
        return country;
    }

    public String getImageURL()
    {
        return imageURL;
    }

    @Override
    public int hashCode()
    {
        return id;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        else if (!(obj instanceof Player))
        {
            return false;
        }
        return id == ((Player) obj).id;
    }
}
