package com.example.snookerapp;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.snookerapp.database.SnookerDatabase;
import com.example.snookerapp.database.entity.CountryEntity;
import com.example.snookerapp.database.entity.EventTypeEntity;
import com.example.snookerapp.database.entity.PlayerEntity;
import com.example.snookerapp.database.entity.PlayerPerformanceEntity;
import com.example.snookerapp.database.entity.RoundEntity;
import com.example.snookerapp.database.entity.SeasonEntity;
import com.example.snookerapp.model.Event;
import com.example.snookerapp.repository.EventRepository;
import com.example.snookerapp.repository.PlayerRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class SnookerApplication extends Application
{
    private SnookerDatabase database;

    @Override
    public void onCreate()
    {
        super.onCreate();

        database = SnookerDatabase.getDatabase(getApplicationContext());;
        boolean isFirstRun = getSharedPreferences("APP_PREFERENCES", MODE_PRIVATE).getBoolean("is_first_run", true);

        if(isFirstRun)
        {
            getSharedPreferences("APP_PREFERENCES", MODE_PRIVATE).edit().putBoolean("is_first_run",false).apply();
            new Thread(this::populateDatabase).start();
        }
        else
        {
            Thread thread = new Thread(() ->
            {
                getCurrentTournament();

                if(checkNetworkConnection())
                {
                    new EventRepository(this).insertEvents();
                }
            });
            thread.start();
        }
    }

    private void populateDatabase()
    {
        insertCountries();
        insertEventTypes();
        insertRounds();
        insertSeasons();
        insertPlayer();

        if(checkNetworkConnection())
        {
            new PlayerRepository(this).insertProPlayers();
            new EventRepository(this).insertEvents();
        }

        getCurrentTournament();
    }

    private void insertCountries()
    {
        List<CountryEntity> countryList = new ArrayList<>();

        countryList.add(new CountryEntity("England"));
        countryList.add(new CountryEntity("Scotland"));
        countryList.add(new CountryEntity("Wales"));
        countryList.add(new CountryEntity("China"));
        countryList.add(new CountryEntity("Thailand"));
        countryList.add(new CountryEntity("Australia"));
        countryList.add(new CountryEntity("Ireland"));
        countryList.add(new CountryEntity("Northern Ireland"));
        countryList.add(new CountryEntity("India"));
        countryList.add(new CountryEntity("Belgium"));
        countryList.add(new CountryEntity("Hong Kong"));
        countryList.add(new CountryEntity("Malta"));
        countryList.add(new CountryEntity("Germany"));
        countryList.add(new CountryEntity("Brazil"));
        countryList.add(new CountryEntity("Iran"));
        countryList.add(new CountryEntity("Canada"));
        countryList.add(new CountryEntity("Norway"));
        countryList.add(new CountryEntity("Pakistan"));
        countryList.add(new CountryEntity("Poland"));
        countryList.add(new CountryEntity("Finland"));
        countryList.add(new CountryEntity("Switzerland"));
        countryList.add(new CountryEntity("Qatar"));
        countryList.add(new CountryEntity("New Zealand"));
        countryList.add(new CountryEntity("UAE"));
        countryList.add(new CountryEntity("Iceland"));
        countryList.add(new CountryEntity("Bahrain"));
        countryList.add(new CountryEntity("Mauritius"));
        countryList.add(new CountryEntity("USA"));
        countryList.add(new CountryEntity("Sweden"));
        countryList.add(new CountryEntity("Afghanistan"));
        countryList.add(new CountryEntity("Belarus"));
        countryList.add(new CountryEntity("Croatia"));
        countryList.add(new CountryEntity("Cyprus"));
        countryList.add(new CountryEntity("Egypt"));
        countryList.add(new CountryEntity("Denmark"));
        countryList.add(new CountryEntity("France"));
        countryList.add(new CountryEntity("Holland"));
        countryList.add(new CountryEntity("Israel"));
        countryList.add(new CountryEntity("Libya"));
        countryList.add(new CountryEntity("Malaysia"));
        countryList.add(new CountryEntity("Mongolia"));
        countryList.add(new CountryEntity("Morocco"));
        countryList.add(new CountryEntity("Oman"));
        countryList.add(new CountryEntity("Saudi Arabia"));
        countryList.add(new CountryEntity("Singapore"));
        countryList.add(new CountryEntity("South Africa"));
        countryList.add(new CountryEntity("Sri Lanka"));
        countryList.add(new CountryEntity("Syria"));
        countryList.add(new CountryEntity("Turkey"));
        countryList.add(new CountryEntity("Romania"));
        countryList.add(new CountryEntity("Austria"));
        countryList.add(new CountryEntity("Latvia"));
        countryList.add(new CountryEntity("Bulgaria"));
        countryList.add(new CountryEntity("Macao"));
        countryList.add(new CountryEntity("Hungary"));
        countryList.add(new CountryEntity("Gibraltar"));
        countryList.add(new CountryEntity("TBD"));
        countryList.add(new CountryEntity("Unknown"));

        database.countryDAO().insert(countryList);
    }

    private void insertEventTypes()
    {
        List<EventTypeEntity> eventTypeList = new ArrayList<>();

        eventTypeList.add(new EventTypeEntity("Ranking"));
        eventTypeList.add(new EventTypeEntity("Invitational"));
        eventTypeList.add(new EventTypeEntity("League"));
        eventTypeList.add(new EventTypeEntity("Qualifying"));
        eventTypeList.add(new EventTypeEntity("Challenge"));
        eventTypeList.add(new EventTypeEntity("Senior"));
        eventTypeList.add(new EventTypeEntity("Ladies"));
        eventTypeList.add(new EventTypeEntity("Team"));
        eventTypeList.add(new EventTypeEntity("Variant"));
        eventTypeList.add(new EventTypeEntity("Six-Red"));
        eventTypeList.add(new EventTypeEntity("Ten-Red"));
        eventTypeList.add(new EventTypeEntity("Shot-Clock"));

        database.eventTypeDAO().insert(eventTypeList);

    }

    private void insertRounds()
    {
        List<RoundEntity> roundList = new ArrayList<>();

        roundList.add(new RoundEntity(1, "Qual Round 1"));
        roundList.add(new RoundEntity(2, "Qual Round 2"));
        roundList.add(new RoundEntity(3, "Qual Round 3"));
        roundList.add(new RoundEntity(4, "Qual Round 4"));
        roundList.add(new RoundEntity(7, "Round 1"));
        roundList.add(new RoundEntity(8, "Round 2"));
        roundList.add(new RoundEntity(9, "Round 3"));
        roundList.add(new RoundEntity(10, "Round 4"));
        roundList.add(new RoundEntity(11, "Round 5"));
        roundList.add(new RoundEntity(13, "Quarterfinals"));
        roundList.add(new RoundEntity(14, "Semifinals"));
        roundList.add(new RoundEntity(15, "Final"));
        roundList.add(new RoundEntity(17, "Champion"));
        roundList.add(new RoundEntity(19, "Round-Robin"));

        database.roundDAO().insert(roundList);
    }

    private void insertSeasons()
    {
        SeasonEntity season = new SeasonEntity(2019, 2020);
        database.seasonsDAO().insert(season);
    }

    private void insertPlayer()
    {
        int seasonID = database.seasonsDAO().getSeasonID(2019);

        PlayerEntity playerTBD = new PlayerEntity();

        playerTBD.setID(376);
        playerTBD.setFirstName("TBD");
        playerTBD.setLastName("TBD");
        playerTBD.setCountryID(database.countryDAO().getIdByName("TBD"));
        database.playerDAO().insert(playerTBD);

        PlayerPerformanceEntity performance = new PlayerPerformanceEntity(376, seasonID, false);
        database.playerPerformanceDAO().insert(performance);
    }

    private void getCurrentTournament()
    {
        List<Event> eventList = database.eventDAO().getEventsDates();

        SharedPreferences sp = getSharedPreferences("APP_PREFERENCES", MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        for (int i = 0; i < eventList.size(); i++)
        {
            String startDateString = eventList.get(i).getStartDate();
            String endDateString = eventList.get(i).getEndDate();

            LocalDate startDate = LocalDate.parse(startDateString);
            LocalDate endDate = LocalDate.parse(endDateString);

            if( (startDate.isBefore(LocalDate.now()) || startDate.isEqual(LocalDate.now())) &&
                    (endDate.isAfter(LocalDate.now()) || endDate.isEqual(LocalDate.now())))
            {
                editor.putInt("current_tournament",eventList.get(i).getId()).apply();
                return;
            }

            editor.putInt("current_tournament", 0).apply();
        }
    }
    private boolean checkNetworkConnection()
    {
        ConnectivityManager cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm != null)
        {
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();
            return networkInfo != null && networkInfo.isConnected();
        }
        return false;
    }
}
